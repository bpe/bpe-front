import {Directive, ElementRef, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {UserRole} from "../models/enums/user-role.enum";
import {AclService} from "../services/acl.service";

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective {
  private roles: UserRole[] = [];

  constructor(
    private aclService: AclService,
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
  }

  /**
   * Displays element only if connected user has one of the roles
   *
   * @param roles array of user roles
   */
  @Input()
  set appHasRole(roles: UserRole[] | UserRole) {
    this.roles = Array.isArray(roles) ? roles : [roles];
    this.checkPermissions();
  }

  private checkPermissions() {
    this.aclService.hasRole(this.roles, null).subscribe(
      (canSee) => {
        if (canSee) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
      }
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminModuleRoutingModule } from './admin-module-routing.module';
import { AdminModuleComponent } from './admin-module.component';
import { UsersComponent } from './components/users/users.component';
import {GlobalComponentsModule} from "../global-components/global-components.module";
import {NgbCollapse, NgbNav, NgbNavItem, NgbNavLink, NgbTooltip} from "@ng-bootstrap/ng-bootstrap";
import { ProjectsComponent } from './components/projects/projects.component';
import { SessionsComponent } from './components/sessions/sessions.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {FormsModule} from "@angular/forms";
import { OffcanvasProjectComponent } from './components/projects/offcanvas-project/offcanvas-project.component';
import { EditSessionModalComponent } from './components/sessions/edit-session-modal/edit-session-modal.component';


@NgModule({
  declarations: [
    AdminModuleComponent,
    UsersComponent,
    ProjectsComponent,
    SessionsComponent,
    OffcanvasProjectComponent,
    EditSessionModalComponent
  ],
    imports: [
        CommonModule,
        AdminModuleRoutingModule,
        GlobalComponentsModule,
        NgbNav,
        NgbNavLink,
        NgbNavItem,
        NgxDatatableModule,
        FormsModule,
        NgbTooltip,
        NgbCollapse,
    ]
})
export class AdminModuleModule { }

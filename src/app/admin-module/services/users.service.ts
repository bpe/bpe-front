import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {User} from "../../models/user";
import {map} from "rxjs";
import {UserRole} from "../../models/enums/user-role.enum";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) {
  }


  getUsers() {
    return this.http.get<User[]>('/users').pipe(map(users => users.map(u => {
      u.role = UserRole[u.role as unknown as keyof typeof UserRole];
      return u;
    })));
  }

  patchUser(id: number, newData: { role: string }) {
    return this.http.patch<User>(`/users/${id}`, newData).pipe(map(u => {
      u.role = UserRole[u.role as unknown as keyof typeof UserRole];
      return u;
    }));
  }
}

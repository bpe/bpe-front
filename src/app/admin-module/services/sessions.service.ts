import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Session} from "../../models/session";
import {forkJoin, map, mergeMap, Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {AppConfigService} from "../../services/app-config.service";

@Injectable({
  providedIn: 'root'
})
export class SessionsService {

  constructor(private http: HttpClient) {
  }

  getTimelineItems() {
    return this.http.get<{ id: string, name: string }[]>("/timeline-items");
  }

  createSession(session: Session): Observable<Session> {
    return this.http.post<Session>("/sessions", {name: session.name, slug: session.slug})
      .pipe(
        mergeMap((s: Session) =>
          forkJoin(session.timelineSteps.map((step: any) =>
            this.http.post(`/sessions/${s.id}/steps`, {
              timelineItem: step.timelineItem.id,
              start: step.start,
              end: step.end
            })
          )).pipe(mergeMap(() => this.getSessionById(s.id)))
        ));
  }

  getSessionById(id: number) {
    return this.http.get<Session>(`/sessions/${id}`);
  }

  getSessions(includeDeleted: boolean = false): Observable<Session[]> {
    return this.http.get<Session[]>("/sessions", {params: {includeDeleted}}).pipe(
      map(sessions => sessions.map(s => {
        s.timelineSteps.forEach(s => {
          s.end = new Date(s.end);
          // s.end = new Date(s.end.getFullYear(), s.end.getMonth(), s.end.getDate() - 1, 23, 59);
          s.start = new Date(s.start)
        })
        return s
      }))
    )
  }

  updateSession(session: Session): Observable<Session> {
    return this.http.patch<Session>(`/sessions/${session.id}`, {
      name: session.name,
      slug: session.slug,
      deleted: session.deleted
    });
  }

  updateTimelineStep(step: {id:number, start: Date, end: Date}) {
    return this.http.patch(`/timeline-steps/${step.id}`, {start: step.start, end: step.end});
  }

  deleteSession(session: Session) {
    return this.http.delete(`/sessions/${session.id}`);
  }

  setActiveSession(sessionId: number) {
    return this.http.put("/settings/active-session-id", {value: sessionId}).pipe(
      tap(_ => {
        AppConfigService.settings.currentSessionId = sessionId;
      })
    )
  }
}

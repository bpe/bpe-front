import {Component} from '@angular/core';
import {ColumnMode} from "@swimlane/ngx-datatable";
import {Project} from "../../../models/project";
import {UsersService} from "../../services/users.service";
import {ProjectsService} from "../../../services/projects.service";
import {Session} from "../../../models/session";
import {OffcanvasProjectComponent} from "./offcanvas-project/offcanvas-project.component";
import {NgbModal, NgbOffcanvas} from "@ng-bootstrap/ng-bootstrap";
import {merge} from "rxjs";
import {AppConfigService} from "../../../services/app-config.service";
import {
  DeleteConfirmModalComponent
} from "../../../global-components/delete-confirm-modal/delete-confirm-modal.component";
import {ToastService} from "../../../services/toast.service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {
  projects: Project[] = [];
  private projectsCache: Project[] = [];
  datatableLoading = false;
  projectsLoading = true;

  sessions!: Session[];
  selectedSession!: Session;

  filterString: string = "";


  protected readonly ColumnMode = ColumnMode;

  constructor(
    private usersService: UsersService,
    private toastService: ToastService,
    private projectsService: ProjectsService,
    private modalService: NgbModal,
    private offcanvasService: NgbOffcanvas) {
  }

  ngOnInit(): void {
    this.projectsLoading = true;
    this.projectsService.getSessions().subscribe(sessions => {
      this.sessions = sessions;
      this.selectedSession = sessions.find(s => s.id === AppConfigService.settings.currentSessionId) || sessions[0];
      this.getProjectsForSession(this.selectedSession);
    })
  }

  getProjectsForSession(s: Session) {
    this.projectsLoading = true;
    this.projectsService.getProjectsForSession(s, true).subscribe((projects: Project[]) => {
      this.projectsCache = projects;
      this.updateFilter(this.filterString || "");
      this.projectsLoading = false;
    });
  }

  updateFilter(event: any) {
    if (!event) {
      this.projects = [...this.projectsCache];
    } else {
      this.projects = this.projectsCache.filter(project => {
        return project.description.toLowerCase().includes(event.toLowerCase()) ||
          project.owner.name.toLowerCase().includes(event.toLowerCase()) ||
          project.workgroup.toLowerCase().includes(event.toLowerCase()) ||
          project.title.toLowerCase().includes(event.toLowerCase())
      });
    }
  }

  selectSession() {
    this.getProjectsForSession(this.selectedSession);
  }

  openProjectOffcanvas(project: Project) {
    document.getElementsByTagName("html")[0]?.classList.add("overflow-hidden");
    const offcanvasRef = this.offcanvasService.open(OffcanvasProjectComponent, {
      position: 'end',
      container: 'body',
      panelClass: "w-90"
    });
    offcanvasRef.componentInstance.project = project;
    merge(offcanvasRef.closed, offcanvasRef.dismissed, offcanvasRef.hidden).subscribe(
      () => {
        document.getElementsByTagName("html")[0]?.classList.remove("overflow-hidden");
      }
    );
  }

  deleteProject(project: Project) {
    const modalRef = this.modalService.open(DeleteConfirmModalComponent);
    modalRef.componentInstance.title = 'Suppression d\'un projet';
    modalRef.componentInstance.message = `le projet ${project.title} de ${project.owner.name}`;

    modalRef.result.then((confirmed) => {
      if (confirmed) {
        this.projectsService.deleteProject(project.id).subscribe(() => {
          this.projectsCache = this.projectsCache.filter(p => p.id !== project.id);
          this.updateFilter(this.filterString);
          this.toastService.success("", `Le projet ${project.title} a bien été supprimé`)
        }, (err) => {
          console.error(err);
          this.toastService.error("", `Une erreur est survenue lors de la suppression du projet ${project.title}`)
        });
      }
    }).catch(_ => null);
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveOffcanvas} from "@ng-bootstrap/ng-bootstrap";
import {Project} from "../../../../models/project";

@Component({
  selector: 'app-offcanvas-project',
  templateUrl: './offcanvas-project.component.html',
  styleUrls: ['./offcanvas-project.component.scss']
})
export class OffcanvasProjectComponent implements OnInit {
  @Input() project!: Project;

  constructor(public activeOffcanvas: NgbActiveOffcanvas) {
  }

  ngOnInit(): void {
    if (!this.project) {
      this.activeOffcanvas.close("No project selected");
    }
  }

}

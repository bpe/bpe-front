import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffcanvasProjectComponent } from './offcanvas-project.component';

describe('OffcanvasProjectComponent', () => {
  let component: OffcanvasProjectComponent;
  let fixture: ComponentFixture<OffcanvasProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OffcanvasProjectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OffcanvasProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

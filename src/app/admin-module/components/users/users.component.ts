import {Component} from '@angular/core';
import {UsersService} from "../../services/users.service";
import {User} from "../../../models/user";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {AuthService} from "../../../services/auth.service";
import {ToastService} from "../../../services/toast.service";
import {UserRole} from "../../../models/enums/user-role.enum";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  public users: User[] = [];
  usersLoading = true;
  protected readonly ColumnMode = ColumnMode;
  datatableLoading = false;
  authenticatedUser?: User;
  filterString = "";
  private usersCache: User[] = [];


  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.authService.getConnectedUser().subscribe(user => {
      this.authenticatedUser = user as User;
    });
    this.getUsers();
  }

  getUsers() {
    this.usersLoading = true;
    this.usersService.getUsers().subscribe((users: User[]) => {
      this.usersCache = users;
      this.updateFilter("");
      this.usersLoading = false;
    });
  }


  updateAdmin(newValue: boolean, row: User, toggleInputElement: HTMLInputElement) {
    this.datatableLoading = true;
    this.usersService.patchUser(row.id, {role: newValue ? "ROLE_ADMIN" : "ROLE_USER"}).subscribe(
      (user: User) => {
        this.datatableLoading = false;
        row = user;
        this.toastService.success("", "Utilisateur modifié");
      },
      (err: Error) => {
        console.error(err);
        this.toastService.error("", "Impossible de modifier l'utilisateur");
        this.datatableLoading = false;
        toggleInputElement.checked = !newValue;
      }
    )
  }

  updateFilter(event: any) {
    if (!event) {
      this.users = [...this.usersCache];
    } else {
      this.users = this.usersCache.filter(user => {
        return user.username.toLowerCase().includes(event.toLowerCase()) ||
          user.name.toLowerCase().includes(event.toLowerCase()) ||
          user.email.toLowerCase().includes(event.toLowerCase())
      });
    }
  }

  protected readonly UserRole = UserRole;
}

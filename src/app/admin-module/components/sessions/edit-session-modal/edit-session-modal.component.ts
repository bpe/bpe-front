import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Session} from "../../../../models/session";
import {ToastService} from "../../../../services/toast.service";
import {SessionsService} from "../../../services/sessions.service";
import {NgForm} from "@angular/forms";
import {forkJoin, mergeMap} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-edit-session-modal',
  templateUrl: './edit-session-modal.component.html',
  styleUrls: ['./edit-session-modal.component.scss']
})
export class EditSessionModalComponent implements OnInit {
  @Input() session: Session = {} as Session;
  errors?: string[];
  displayBottomErrorMessage = false;

  constructor(public activeModal: NgbActiveModal, private toastService: ToastService, private sessionsService: SessionsService) {
  }

  save(_: NgForm) {
    const errors = this.validateSession();
    if (errors.length > 0) {
      this.errors = errors;
      this.displayBottomErrorMessage = true;
      setTimeout(() => {
        this.displayBottomErrorMessage = false;
      }, 3000);
      return;
    }

    this.errors = undefined;

    //TODO : update only if changed
    if (this.session.id) {
      // Update
      this.sessionsService.updateSession(this.session)
        .pipe(
          mergeMap((s) =>
            forkJoin(
              this.session.timelineSteps.map(step => this.sessionsService.updateTimelineStep(step))
            )
              .pipe(map(_ => s))
          )
        )
        .subscribe(
          {
            next: (session: Session) => {
              this.toastService.success("", "Session modifiée");
              this.activeModal.close(session);
            },
            error: (err: any) => {
              console.error(err);
              this.toastService.error("", "Erreur lors de la modification de la session, " + err.error);
            }
          });
    } else {
      // Create
      this.sessionsService.createSession(this.session).subscribe(
        {
          next: (session: Session) => {
            this.toastService.success("", "Session créée");
            this.activeModal.close(session);
          },
          error: (err: any) => {
            console.error(err);
            this.toastService.error("", "Erreur lors de la création de la session, " + err.error);
          }
        });
    }
  }

  ngOnInit(): void {
    const defaultItems = ["DEPOSIT", "PROJECT_EXAMINATION", "VOTING", "REALIZATION"];
    if (!this.session.id) {
      this.sessionsService.getTimelineItems().subscribe(timelineItems => {
        //Template is : DEPOSIT, PROJECT_EXAMINATION, VOTING, REALIZATION
        this.session.timelineSteps = defaultItems.map(it => {
          const ti = timelineItems.find(t => t.id === it);
          if (ti) {
            return {timelineItem: ti, start: undefined, end: undefined, id: undefined} as any;
          } else {
            return null;
          }
        }).filter(it => it !== null);
      });
    } else {
      // Set default order for timeline steps using defaultItems
      this.session.timelineSteps = this.session.timelineSteps.sort((a, b) => {
        return defaultItems.indexOf(a.timelineItem.id) - defaultItems.indexOf(b.timelineItem.id);
      });
    }
  }


  private validateSession() {
    const errors = [];
    if (!this.session.name) {
      errors.push("Le nom de la session est obligatoire");
    }
    if (!this.session.slug) {
      errors.push("Le slug de la session est obligatoire");
    } else {
      if (!this.session.slug.match(/^[a-z0-9]+(?:-[a-z0-9]+)*$/)) {
        errors.push("Le slug de la session ne doit contenir que des caractères alphanumériques et des tirets");
      }
    }

    this.session.timelineSteps.forEach((ts, index) => {
      if (!ts.start) {
        errors.push(`La date de début de l'étape ${ts.timelineItem.name} est obligatoire`);
      }
      if (!ts.end) {
        errors.push(`La date de fin de l'étape ${ts.timelineItem.name} est obligatoire`);
      }
      if (ts.start > ts.end) {
        errors.push(`La date de début de l'étape ${ts.timelineItem.name} doit être antérieure à la date de fin`);
      }
      if (index > 0) {
        const previousStep = this.session.timelineSteps[index - 1];
        if (previousStep.end > ts.start) {
          errors.push(`${previousStep.timelineItem.name} doit être terminé avant le début de ${ts.timelineItem.name}`);
        }
      }
    })

    return errors;
  }
}

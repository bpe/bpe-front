import {Component, OnInit} from '@angular/core';
import {Session} from "../../../models/session";
import {ToastService} from "../../../services/toast.service";
import {AppConfigService} from "../../../services/app-config.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {EditSessionModalComponent} from "./edit-session-modal/edit-session-modal.component";
import {SessionsService} from "../../services/sessions.service";
import {ProjectsService} from "../../../services/projects.service";
import {
  DeleteConfirmModalComponent
} from "../../../global-components/delete-confirm-modal/delete-confirm-modal.component";

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {
  sessions: Session[] = [];
  activeSessionId!: number;
  isCollapsed: Map<number, boolean> = new Map<number, boolean>();

  projectsCountForSession = new Map<number, number>();

  constructor(
    private toastService: ToastService,
    private sessionsService: SessionsService,
    private projectsService: ProjectsService,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.activeSessionId = AppConfigService.settings.currentSessionId;
    this.getSessions();
  }

  editSession(session: Session) {
    const modalInstance = this.modalService.open(EditSessionModalComponent);
    modalInstance.result.then((_) => {
      this.getSessions();
    }).catch((_) => {
      this.getSessions();
    });

    modalInstance.componentInstance.session = session;
  }

  createSession() {
    this.modalService.open(EditSessionModalComponent).result.then((_) => {
      this.getSessions();
    }).catch((_) => {});
  }

  deleteSession(session: Session) {
    const modalRef = this.modalService.open(DeleteConfirmModalComponent);
    modalRef.componentInstance.title = 'Suppression d\'une session';
    modalRef.componentInstance.message = `la session ${session.name}`;

    modalRef.result.then((confirmed) => {
      if (confirmed) {
        this.sessionsService.deleteSession(session).subscribe(() => {
          this.sessions = this.sessions.filter(s => s.id !== session.id);
        });
      }
    }).catch(_ => null);
  }

  private getSessions() {
    this.sessionsService.getSessions(true).subscribe({
      next: (sessions: Session[]) => {
        this.sessions = sessions;
        this.sessions.forEach(s => {
          this.projectsService.getProjectsForSession(s).subscribe(projects => {
            this.projectsCountForSession.set(s.id, projects.length);
          });
          this.isCollapsed.set(s.id, false);
        })
      },
      error: err => {
        console.error(err);
        this.toastService.error("", "Error while retrieving sessions");
      }
    });
  }

  activateSession(session: Session) {
    this.sessionsService.setActiveSession(session.id).subscribe(
      _ => {
        this.activeSessionId = AppConfigService.settings.currentSessionId;
      }
    )
  }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminModuleComponent} from './admin-module.component';
import {UsersComponent} from "./components/users/users.component";
import {SessionsComponent} from "./components/sessions/sessions.component";
import {ProjectsComponent} from "./components/projects/projects.component";

const routes: Routes = [
  {path: '', redirectTo: 'projects', pathMatch: 'full'},
  {
    path: '', component: AdminModuleComponent, children: [
      {path: 'users', component: UsersComponent},
      {path: 'projects', component: ProjectsComponent},
      {path: 'sessions', component: SessionsComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminModuleRoutingModule {
}

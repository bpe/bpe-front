import {Injectable} from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppConfigService} from "../services/app-config.service";
import {ToastService} from "../services/toast.service";

/**
 * Interceptor that catches API calls errors and displays a toast for 403s and unreachable server.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private toastService: ToastService, private router: Router) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(err => {
        if (err.status === 0 && request.url.includes(AppConfigService.settings.apiURL)) {
          this.toastService.error('', 'Serveur injoignable');
        }
        if (err.status === 403) {
          if (err.error && err.error.code === 'UNAUTHORIZED') {
            this.toastService.warning('Action interdite', err.error.message);
          }
        }
        return throwError(err);
      })
    );
  }
}

import {Injectable} from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import {BehaviorSubject, throwError} from 'rxjs';
import {catchError, filter, switchMap, take, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppConfigService} from "../services/app-config.service";
import {AuthService} from "../services/auth.service";
import {ToastService} from "../services/toast.service";

/**
 * Interceptor that adds backend access token to API requests.
 * If access token has expired, it triggers refresh using AuthService.
 * If refresh token has expired it redirects to application login page.
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  /**
   * Private subject to handle queued requests during access token refresh.
   *
   * @private
   */
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private authService: AuthService, private router: Router, private toastService: ToastService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler) {
    // Request for other service
    if (request.url.indexOf(AppConfigService.settings.apiURL) < 0) {
      return next.handle(request);
    }
    request = this.addAccessToken(request);
    if (this.authService.accessTokenHasExpired() && !request.url.includes('refresh') && !request.url.includes('login')) {
      return this.handleRefresh(request, next);
    }
    return next.handle(request).pipe(
      catchError(err => {
        if (request.url.includes('login')) {
          return throwError(err);
        }
        if (request.url.includes('refresh')) {
          this.authService.logout();
        }
        if (err && (err.status === 401)) {
          return this.handleRefresh(request, next);
        }
        return throwError(err);
      })
    );
  }

  /**
   * Add Authorization header to request
   *
   * @param request
   * @private
   */
  private addAccessToken(request: HttpRequest<any>): HttpRequest<any> {
    if (!this.authService.getAccessToken() || (request.url.includes('auth') && !request.url.includes('/me') && !request.url.includes("/tokens/"))) {
      return request;
    }
    return request.clone({
      headers: request.headers.set('Authorization', 'Bearer ' + this.authService.getAccessToken())
    });
  }

  /**
   * Function called if access token is invalid.
   * Tries to refresh access token and play / replay request.
   *
   * @param request
   * @param next
   * @private
   */
  private handleRefresh(request: HttpRequest<unknown>, next: HttpHandler) {
    if (!this.refreshTokenSubject.getValue()) {
      this.refreshTokenSubject.next(true);
      this.authService.refreshAccessToken().subscribe(
        () => this.refreshTokenSubject.next(false)
      );
    }
    return this.refreshTokenSubject.pipe(
      filter(refresh => refresh === false),
      take(1),
      switchMap(() => next.handle(this.addAccessToken(request))),
      catchError((err) => {
        if (err.status === 401) {
          this.authService.logout();
          this.toastService.info("", "Session expirée");
        }
        return throwError(err);
      })
    );
  }
}

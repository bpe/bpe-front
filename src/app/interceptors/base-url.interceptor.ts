import {Injectable} from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {AppConfigService} from "../services/app-config.service";

/**
 * Interceptor that adds backend base API URL using AppConfigService apiURL value.
 */
@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Request for other service
    if (request.url.indexOf('://') >= 0) {
      return next.handle(request);
    }
    if (!AppConfigService.settings || !AppConfigService.settings.apiURL) {
      console.error('Missing API URL', 'Tried to reach', request.url);
      return EMPTY;
    }
    const newReq = request.clone({url: AppConfigService.settings.apiURL + request.url});
    return next.handle(newReq);
  }
}

import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterLink} from "@angular/router";
import {AboutComponent} from './components/about/about.component';
import {
  ProjectsRealisationsComponent
} from './components/projects/projects-realisations/projects-realisations.component';
import {NgOptimizedImage, registerLocaleData} from "@angular/common";
import {TimelineComponent} from './components/timeline/timeline.component';
import {RulesComponent} from './components/rules/rules.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from "@angular/common/http";
import {AppRoutingModule} from './app-routing.module';
import {AccessForbiddenComponent} from "./components/helpers/access-forbidden/access-forbidden.component";
import {PageNotFoundComponent} from "./components/helpers/page-not-found/page-not-found.component";
import {HomeComponent} from './components/home/home.component';
import {FormsModule} from "@angular/forms";
import {BaseUrlInterceptor} from "./interceptors/base-url.interceptor";
import localeFr from '@angular/common/locales/fr';
import {ToastComponent} from "./components/toast/toast.component";
import {AppConfigService} from "./services/app-config.service";
import {TokenInterceptor} from "./interceptors/token.interceptor";
import {ErrorInterceptor} from "./interceptors/error.interceptor";
import { LoginCasComponent } from './components/login-cas/login-cas.component';
import {GlobalComponentsModule} from "./global-components/global-components.module";
import { LegalComponent } from './components/legal/legal.component';

export const initializeApp = (appConfigService: AppConfigService) => () => appConfigService.load();

@NgModule({ declarations: [
        AppComponent,
        NavbarComponent,
        AboutComponent,
        ProjectsRealisationsComponent,
        TimelineComponent,
        RulesComponent,
        AccessForbiddenComponent,
        PageNotFoundComponent,
        HomeComponent,
        ToastComponent,
        LoginCasComponent,
        LegalComponent,
    ],
    exports: [],
    bootstrap: [AppComponent], imports: [BrowserModule,
        BrowserAnimationsModule,
        NgbModule,
        RouterLink,
        NgOptimizedImage,
        AppRoutingModule,
        FormsModule,
        GlobalComponentsModule], providers: [
        { provide: LOCALE_ID, useValue: 'fr' },
        { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfigService], multi: true },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BaseUrlInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true
        },
        provideHttpClient(withInterceptorsFromDi()),
    ] })
export class AppModule {
  constructor() {
    registerLocaleData(localeFr);
  }
}

import {ProjectStatus} from "./project-status";
import {User} from "./user";
import {ProjectAttachment} from "./project-attachment";
import {ProjectAttachmentType} from "./enums/project-attachment-type";
import {ProjectComment} from "./project-comment";

export class Project {
  constructor(p: Project) {
    this.id = p.id;
    this.session = (p.session as any)?.id;
    this.title = p.title;
    this.description = p.description;
    this.details = p.details;
    this.slug = p.slug;
    this.category = p.category;
    this.status = p.status;
    this.budget = p.budget;
    this.allocatedBudget = p.allocatedBudget;
    this.owner = p.owner;
    this.phone = p.phone;
    this.workgroup = p.workgroup;
    this.comments = p.comments;
    this.attachments = p.attachments;
    this.votantsCount = p.votantsCount;
    this.realised = p.realised;
  }

  id!: number;
  session!: number;
  title!: string;
  description!: string;
  details!: string;
  slug!: string;

  category!: { id: number, name: string };
  status!: ProjectStatus;

  budget!: number;
  allocatedBudget!: number;

  owner!: User;

  phone!: string;
  workgroup!: string;

  comments?: ProjectComment[]

  attachments?: ProjectAttachment[]

  votantsCount!: number;
  realised!: boolean;

  getHeaderImage(): ProjectAttachment | undefined {
    return this.attachments?.find(a => a.type === ProjectAttachmentType.HEADER_IMAGE);
  };
}

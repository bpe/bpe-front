import {UserRole} from "./enums/user-role.enum";

export interface User  {
  id: number;
  username: string;
  name: string;
  active: boolean;
  email: string;
  profile: string;
  role: UserRole;
}

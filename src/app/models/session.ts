export interface Session {
  deleted: boolean;
  id: number;
  name: string;
  slug: string;

  timelineSteps: {
    id: number;
    timelineItem: {
      id: string;
      name: string;
    },
    start: Date;
    end: Date;
  }[]
}

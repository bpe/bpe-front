export enum UserRole {
  ROLE_USER = "Utilisateur",
  ROLE_ADMIN = 'Administrateur',
}

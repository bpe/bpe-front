import {ProjectAttachmentType} from "./enums/project-attachment-type";

export interface ProjectAttachment {
  type: ProjectAttachmentType;
  description: string;
  resourcePath: string;
  id: string;
  fileType: string;
}

export interface ProjectComment {
  user: string,
  content: string,
  date: Date,
  id: number,
  published: boolean
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionCalendarCardComponent } from './session-calendar-card.component';

describe('SessionCalendarCardComponent', () => {
  let component: SessionCalendarCardComponent;
  let fixture: ComponentFixture<SessionCalendarCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionCalendarCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SessionCalendarCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

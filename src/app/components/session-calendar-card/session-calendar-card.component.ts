import {Component, Input, OnChanges} from '@angular/core';
import {Session} from "../../models/session";

@Component({
  selector: 'app-session-calendar-card',
  templateUrl: './session-calendar-card.component.html',
  styleUrls: ['./session-calendar-card.component.scss']
})
export class SessionCalendarCardComponent implements OnChanges {
  @Input() displayCTA: boolean = true;
  @Input() session?: Session;
  activeStepNumber?: number;
  activeStep?: any;
  sessionEnded = false;
  @Input() shadowed = true;

  ngOnChanges() {
    this.activeStep = undefined;
    this.activeStepNumber = undefined;
    this.sessionEnded = false;

    if (!this.session || this.session.timelineSteps.length < 1) {
      return;
    }


    const now = new Date(Date.now());
    // Find current step, first predicate : current Date is between start & end
    let step = this.session?.timelineSteps.find((step) => now > step.start && now < step.end);

    let pastStep = undefined;
    if (!step) {
      // Find last past step, second predicate : current Date is after end
      this.session.timelineSteps.forEach(s => {
        if (s.end < now) {
          pastStep = s;
        }
      })
    }

    if (pastStep != undefined && pastStep != this.session.timelineSteps[this.session.timelineSteps.length - 1]) {
      step = pastStep;
    }


    if (step) {
      this.activeStep = step;
      this.activeStepNumber = (this.session?.timelineSteps.indexOf(step) || 0) + 1;
    } else {
      // Take last one if end < now
      const last = this.session.timelineSteps[this.session.timelineSteps.length - 1];
      if (last.end < now) {
        this.activeStep = last;
        this.activeStepNumber = this.session.timelineSteps.length;
      }
    }

    if (this.activeStepNumber == this.session.timelineSteps.length && this.activeStep.end < new Date()) {
      this.sessionEnded = true;
    }
  }

}

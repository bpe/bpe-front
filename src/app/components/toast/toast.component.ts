import {Component, TemplateRef} from '@angular/core';
import {fadeInAnimation} from "../../../animations";
import {ToastService} from "../../services/toast.service";


@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  animations: [fadeInAnimation]
})
export class ToastComponent {

  constructor(public toastService: ToastService) {
  }

  isTemplate(item: any) {
    return item instanceof TemplateRef;
  }

  getTemplate(content: string | ArrayBuffer | DocumentFragment) {
    return content as unknown as TemplateRef<any>;
  }

  closeToast(toast: any) {
    this.toastService.remove(toast);
  }
}

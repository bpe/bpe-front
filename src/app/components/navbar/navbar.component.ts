import {Component, OnInit, TemplateRef} from '@angular/core';
import {NgbOffcanvas} from "@ng-bootstrap/ng-bootstrap";
import {Session} from "../../models/session";
import {ProjectsService} from "../../services/projects.service";
import {AuthService} from "../../services/auth.service";
import {User} from "../../models/user";
import {UserRole} from "../../models/enums/user-role.enum";
import {UserVotesModalComponent} from "../../global-components/user-votes-modal/user-votes-modal.component";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  sessions: Session[] = []
  currentSession?: Session;
  authenticatedUser!: User | null;

  protected readonly UserRole = UserRole;

  constructor(
    private offcanvasService: NgbOffcanvas,
    private authService: AuthService,
    private projectsService: ProjectsService) {
  }

  ngOnInit(): void {
    this.projectsService.getSessions().subscribe(sessions =>
      this.sessions = sessions.sort((a, b) => b.id - a.id)
    )
    this.projectsService.getCurrentSession().subscribe(s => this.currentSession = s);

    this.authService.getConnectedUser().subscribe(user => this.authenticatedUser = user)

    this.authService.loginEvent.subscribe(() => {
      this.authService.getConnectedUser().subscribe(user => this.authenticatedUser = user)
    });
    this.authService.logoutEvent.subscribe(() => {
      this.authenticatedUser = null;
    });
  }

  openOffcanvas(content: TemplateRef<any>) {
    this.offcanvasService.open(content, {ariaLabelledBy: 'offcanvasNavbarLabel', position: "end", container: 'body'});
  }

  goToLogin() {
    this.authService.goToLogin();
  }

  logout() {
    this.authService.logout();
  }

  protected readonly Date = Date;

  isPastSession(session: Session) {
    return session.timelineSteps[session.timelineSteps.length - 1].end < new Date();
  }

  showProjectsLink() {
    // Is user is ADMIN show link
    if (this.authenticatedUser?.role === UserRole.ROLE_ADMIN) {
      return true;
    }

    // If current session is at least in VOTING phase show link
    if (this.currentSession) {
      const votingStep = this.currentSession?.timelineSteps?.find(step => step.timelineItem.id === "VOTING");
      if (votingStep && votingStep.start <= new Date()) {
        return true;
      }
    }

    return false;
  }

  isVoteOpened() {
    if (!this.currentSession) {
      return false;
    }
    const votingStep = this.currentSession?.timelineSteps?.find(step => step.timelineItem.id === "VOTING");
    return votingStep && votingStep.start <= new Date() && votingStep.end >= new Date();
  }

  openVotesModal() {
    this.offcanvasService.open(UserVotesModalComponent, {ariaLabelledBy: 'user votes offcanvas', position: "bottom", container: 'body', panelClass: 'h-auto'});
  }
}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, UrlTree} from '@angular/router';
// import {AuthService} from '../../services/class-services/auth.service';

@Component({
  selector: 'app-access-forbidden',
  templateUrl: './access-forbidden.component.html',
  styleUrls: ['./access-forbidden.component.css']
})
export class AccessForbiddenComponent implements OnInit {
  parentRouteDetails?: string | UrlTree;
  expectedRights: any;
  userRights: any;
  triedURL: any = 'unknown';

  constructor(private route: ActivatedRoute, private router: Router, /*private authService: AuthService*/) {
    router.events.subscribe(
      () => {
        if (router.getCurrentNavigation()?.extras && router.getCurrentNavigation()?.extras.state) {
          this.expectedRights = router.getCurrentNavigation()?.extras?.state?.['expected'];
          this.triedURL = router.getCurrentNavigation()?.extras?.state?.['url'];
        }

        const previousNav = router.getCurrentNavigation()?.previousNavigation;
        if (previousNav) {
          this.parentRouteDetails = router.getCurrentNavigation()?.initialUrl;
        } else {
          this.parentRouteDetails = 'unknown previous route';
        }

        // this.authService.getConnectedUser().subscribe(
        //   u => {this.userRights = u.roles; }
        // );
      }
    );
  }

  ngOnInit(): void {
  }

}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, UrlTree} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
  routeDetails?: string;
  parentRouteDetails?: string | UrlTree;

  constructor(private route: ActivatedRoute, private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.update();
    // this.router.events.subscribe(
    //   (e) => {if (e instanceof NavigationEnd) { this.update(); } }
    // );
  }

  private update() {
    this.routeDetails = this.route.toString();
    if (this.routeDetails.includes('404')) {
      this.routeDetails += ' ' + location.pathname;
    }
    let previousNav = null;
    if (this.router.getCurrentNavigation()) {
      previousNav = this.router.getCurrentNavigation()?.previousNavigation;
    }
    if (previousNav) {
      this.parentRouteDetails = this.router.getCurrentNavigation()?.initialUrl;
    }
  }
}

import {Component, OnInit} from '@angular/core';
import {Session} from "../../models/session";
import {ProjectsService} from "../../services/projects.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentSession?: Session;
  displayProjectsCarrousel = true;
  activeStep?: any;

  constructor(private projectsService: ProjectsService) {
  }

  ngOnInit() {
    this.projectsService.getCurrentSession().subscribe(s => {
      this.currentSession = s;

      // Check if current session is at least in VOTING phase
      const votingStep = s?.timelineSteps?.find(step => step.timelineItem.id === "VOTING");
      if (votingStep && votingStep.start > new Date()) {
        this.displayProjectsCarrousel = false;
      }

      // Set active step
      this.activeStep = s?.timelineSteps?.find(step => step.start <= new Date() && step.end >= new Date());
    })
  }

}

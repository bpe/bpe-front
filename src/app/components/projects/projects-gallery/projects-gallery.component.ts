import {Component, OnInit} from '@angular/core';
import {Session} from "../../../models/session";
import {Project} from "../../../models/project";
import {ProjectsService} from "../../../services/projects.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-projects-gallery',
  templateUrl: './projects-gallery.component.html',
  styleUrls: ['./projects-gallery.component.scss']
})
export class ProjectsGalleryComponent implements OnInit {
  session?: Session;
  projects?: Project[];
  detailedView = true;

  constructor(
    private projectsService: ProjectsService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has("slug")) {
        this.router.navigate(['/404'], {skipLocationChange: true})
      }
      this.projectsService.getSession(paramMap.get('slug') || '').subscribe({
        next: session => {
          this.session = session;
          this.projectsService.getProjectsForSession(session).subscribe(projects => {
            this.projects = this.shuffle(projects);
          })
        },
        error: err => {
          console.error(err);
          this.router.navigate(['/404'], {skipLocationChange: true})
        }
      })
    })
  }

  private shuffle([...arr]) {
    let m = arr.length;
    while (m) {
      const i = Math.floor(Math.random() * m--);
      [arr[m], arr[i]] = [arr[i], arr[m]];
    }
    return arr;
  };
}

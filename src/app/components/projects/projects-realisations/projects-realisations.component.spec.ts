import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsRealisationsComponent } from './projects-realisations.component';

describe('ProjectsRealisationsComponent', () => {
  let component: ProjectsRealisationsComponent;
  let fixture: ComponentFixture<ProjectsRealisationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectsRealisationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectsRealisationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

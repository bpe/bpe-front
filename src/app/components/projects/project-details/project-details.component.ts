import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ProjectsService} from "../../../services/projects.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Project} from "../../../models/project";
import {Session} from "../../../models/session";
import {ProjectsCarrouselComponent} from "../projects-carrousel/projects-carrousel.component";
import {ProjectAttachmentType} from "../../../models/enums/project-attachment-type";
import {User} from "../../../models/user";
import {AuthService} from "../../../services/auth.service";
import {UserRole} from "../../../models/enums/user-role.enum";
import {ToastService} from "../../../services/toast.service";
import {ProjectAttachment} from "../../../models/project-attachment";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ResultMailSenderComponent} from "../../../global-components/result-mail-sender/result-mail-sender.component";
import {VotesService} from "../../../services/votes.service";

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectDetailsComponent implements OnInit {
  @ViewChild("carrousel", {static: false}) projectsCarrousel?: ProjectsCarrouselComponent;

  @Input() project!: Project;
  @Input() compact: boolean = false;

  session?: Session;
  authenticatedUser?: User;

  protected readonly UserRole = UserRole;
  isVotingActiveOnSession = false;

  constructor(
    private projectsService: ProjectsService,
    private router: Router,
    private authService: AuthService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private voteService: VotesService,
    private activatedRoute: ActivatedRoute) {
    this.voteService.newVoteCountForProject$.subscribe({
      next: (project) => {
        if (this.project && project.id === this.project.id) {
          this.project.votantsCount = project.votantsCount;
        }
      }
    })
  }

  ngOnInit(): void {
    if (!this.project) {
      this.activatedRoute.paramMap.subscribe(paramMap => {
        if (!paramMap.has("id")) {
          this.router.navigate(['/404'], {skipLocationChange: true})
        }
        this.projectsService.getProject(parseInt(paramMap.get('id') || '')).subscribe({
          next: project => {
            this.project = project;
            this.projectsService.getSession(project.session).subscribe({
                next: (session: Session) => {
                  this.session = session;
                  this.isVotingActiveOnSession = session.timelineSteps?.find(step => step.timelineItem.id === "VOTING" && step.start <= new Date() && step.end >= new Date()) !== undefined;
                }
              }
            );
            this.projectsCarrousel?.getRandomProjects();
          },
          error: err => {
            console.error(err);
            this.router.navigate(['/404'], {skipLocationChange: true})
          }
        })
      })
    } else {
      this.projectsService.getSession(this.project.session).subscribe({
          next: (session: Session) => {
            this.session = session;
          }
        }
      );
      this.projectsCarrousel?.getRandomProjects();
    }

    this.authService.getConnectedUser().subscribe(user => {
      this.authenticatedUser = user || undefined;
    })
  }

  getProjectImages() {
    const res = this.project.attachments?.filter(a => a.type === ProjectAttachmentType.IMAGE);
    if (!res || res.length === 0) {
      return undefined;
    }
    return res;
  }

  getFileAttachments() {
    // Put the optional attachments at the end and budget in second place
    return this.project.attachments?.filter(a =>
      [ProjectAttachmentType.BUDGET, ProjectAttachmentType.DESCRIPTION, ProjectAttachmentType.OPTIONAL].includes(a.type))
      .sort((a, b) => {
        if (a.type === ProjectAttachmentType.DESCRIPTION) {
          return -1;
        }
        if (b.type === ProjectAttachmentType.BUDGET) {
          return 1;
        }
        if (a.type === ProjectAttachmentType.OPTIONAL) {
          return 1;
        }
        if (b.type === ProjectAttachmentType.OPTIONAL) {
          return -1;
        }
        return 0;
      });
  };

  getProjectAchievements() {
    const res = this.project.attachments?.filter(a => a.type === ProjectAttachmentType.ACHIEVEMENT);
    if (!res || res.length === 0) {
      return undefined;
    }
    return res;
  }

  getBudgetFile() {
    return this.project.attachments?.find(a => a.type === ProjectAttachmentType.BUDGET);
  }

  userCanEdit() {
    // If the user is not authenticated or not owner, he can't edit
    if (!this.authenticatedUser || !this.project || !this.project.owner) {
      return false;
    }
    if (this.authenticatedUser.id !== this.project.owner.id) {
      return false;
    }

    // If session is in DEPOSIT phase -> user can edit
    if (this.session?.timelineSteps?.find(step => step.timelineItem.id === "DEPOSIT" && step.start <= new Date() && step.end >= new Date())) {
      return true;
    }

    // If session is in PROJECT_EXAMINATION phase && project status is ACTION_NEEDED -> user can edit
    if (this.session?.timelineSteps?.find(step => step.timelineItem.id === "PROJECT_EXAMINATION" && step.start <= new Date() && step.end >= new Date())) {
      return this.project.status === "ACTION_NEEDED";
    }
    return false;
  }


  updateProjectStatus() {
    this.projectsService.updateProjectStatus(this.project.id, this.project.status).subscribe({
      next: () => {
        this.toastService.success('', "Statut du projet mis à jour");
      },
      error: err => {
        console.error(err);
        this.toastService.error('', "Erreur lors de la mise à jour du statut du projet");
      }
    });
  }

  updateAllocatedBudget() {
    this.projectsService.updateAllocatedBudget(this.project.id, this.project.allocatedBudget).subscribe({
      next: () => {
        this.toastService.success('', "Budget alloué mis à jour");
      },
      error: (err: any) => {
        console.error(err);
        this.toastService.error('', "Erreur lors de la mise à jour du budget alloué");
      }
    });
  }

  updateProjectRealised() {
    this.projectsService.updateProjectRealised(this.project.id, this.project.realised).subscribe({
      next: () => {
        this.toastService.success('', `Projet marqué comme ${this.project.realised ? "réalisé" : "non réalisé"}`);
      },
      error: (err: any) => {
        console.error(err);
        this.toastService.error('', "Erreur lors de la mise à jour de l'état de réalisation du projet");
      }
    });
  }

  isImage(achievement: ProjectAttachment) {
    return ["png", "jpg", "jpeg", "gif"].includes(achievement.fileType);
  }

  openMailPreview() {
    const modalRef = this.modalService.open(ResultMailSenderComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.project = this.project;
  }

  login() {
    this.authService.goToLogin(this.router.url);
  }
}

import {Component, Input} from '@angular/core';
import {Project} from "../../../models/project";
import {VotesService} from "../../../services/votes.service";

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent {
  @Input() project!: Project;
  @Input() showDetails = true;

  constructor(private voteService: VotesService) {
    this.voteService.newVoteCountForProject$.subscribe({
      next: (project) => {
        if (this.project && project.id === this.project.id) {
          this.project.votantsCount = project.votantsCount;
        }
      }
    })
  }
}

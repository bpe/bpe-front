import {Component, Input, OnInit} from '@angular/core';
import {ProjectsService} from "../../../services/projects.service";
import {Project} from "../../../models/project";
import {Session} from "../../../models/session";
import {switchMap} from "rxjs";

@Component({
  selector: 'app-projects-carrousel',
  templateUrl: './projects-carrousel.component.html',
  styleUrls: ['./projects-carrousel.component.scss']
})
export class ProjectsCarrouselComponent implements OnInit {
  projects: Project[] = [];
  private projects_cache: Project[] = [];
  @Input() session?: Session;
  @Input() showDetails = true;

  constructor(
    private projectsService: ProjectsService
  ) {
  }

  ngOnInit(): void {
    this.projectsService.getCurrentSession().pipe(
      switchMap(s => this.projectsService.getProjectsForSession(this.session ? this.session : s))
    ).subscribe(
      projects => {
        this.projects_cache = projects;
        this.getRandomProjects();
      })
  }


  getRandomProjects() {
    this.projects = [];
    const randomIndices = new Set<number>();
    const randomItemsCount = this.projects_cache.length > 4 ? 4 : this.projects_cache.length;
    while (randomIndices.size < randomItemsCount) {
      randomIndices.add(Math.floor(Math.random() * this.projects_cache.length))
    }
    randomIndices.forEach(i => {
      this.projects.push(this.projects_cache[i])
    })
  }
}

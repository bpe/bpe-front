import {Component, Input} from '@angular/core';
import {Session} from "../../models/session";

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent {
  @Input() session?: Session;
  now = new Date(Date.now());
}

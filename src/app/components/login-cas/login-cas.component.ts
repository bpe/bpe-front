import {Component} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastService} from "../../services/toast.service";

@Component({
  selector: 'app-login-cas',
  templateUrl: './login-cas.component.html',
  styleUrls: ['./login-cas.component.scss']
})
export class LoginCasComponent {
  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastService: ToastService) {
    this.activatedRoute.queryParams.subscribe(params => {
      const ticket = params['ticket'];
      let redirect = params['redirect']?.split(',') || ['/'];
      redirect = redirect.filter((item: string) => item && item.length > 0)
      if (redirect.length == 0) {
        redirect = ['/']
      }

      if (ticket) {
        this.authService.login(ticket, params['redirect']).subscribe({
            next: () => {
              this.authService.getConnectedUser().subscribe(user => {
                this.toastService.success("", "Bienvenue " + user?.name);
                this.router.navigate(redirect);
              });
            },
            error: (err) => {
              console.error(err);
              this.toastService.error("", "Une erreur est survenue lors de la connexion.");
              this.router.navigate(['/'])
            },
          }
        );
      }
    });
  }
}

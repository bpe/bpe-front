import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCasComponent } from './login-cas.component';

describe('LoginCAsComponent', () => {
  let component: LoginCasComponent;
  let fixture: ComponentFixture<LoginCasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginCasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginCasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ToastService} from "../services/toast.service";
import {AclService} from "../services/acl.service";

@Injectable({
  providedIn: 'root'
})
export class ACLGuard  {

  constructor(
    private ACLService: AclService,
    private toastService: ToastService,
    private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkRights(route);
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkRights(childRoute);
  }

  private checkRights(route: ActivatedRouteSnapshot) {
    if (!route.data || !route.data["roles"]) {
      // block by default
      return false;
    }

    return this.ACLService.hasRole(route.data["roles"], route.url.join('/')).pipe(
      tap((isAuthorized: boolean) => {
        if (!isAuthorized) {
          let expected = '';
          route.data['roles'].forEach((r: string) => expected += r + ' ');
          // this.toastService.error('Accès refusé', `Droits insuffisants (${expected} attendu)`);
          this.router.navigate(['403'], {
            replaceUrl: false,
            skipLocationChange: true,
            state: {expected, url: route.url.toString()}
          });
        }
      })
    );
  }
}

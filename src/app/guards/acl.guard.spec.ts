import { TestBed } from '@angular/core/testing';

import { ACLGuard } from './acl.guard';

describe('ACLGuard', () => {
  let guard: ACLGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ACLGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PageNotFoundComponent} from "./components/helpers/page-not-found/page-not-found.component";
import {HomeComponent} from "./components/home/home.component";
import {ProjectDetailsComponent} from "./components/projects/project-details/project-details.component";
import {ProjectsGalleryComponent} from "./components/projects/projects-gallery/projects-gallery.component";
import {LoginCasComponent} from "./components/login-cas/login-cas.component";
import {UserRole} from "./models/enums/user-role.enum";
import {ACLGuard} from "./guards/acl.guard";
import {AccessForbiddenComponent} from "./components/helpers/access-forbidden/access-forbidden.component";
import {ProjectEditComponent} from "./global-components/project-edit/project-edit.component";
import {LegalComponent} from "./components/legal/legal.component";


const routes: Routes = [
  {path: '', component: HomeComponent, data: {title: 'Accueil'}},
  {path: 'sessions/:slug', component: ProjectsGalleryComponent, data: {title: "Liste des projets"}},
  {path: 'projects/new', component: ProjectEditComponent, data: {title: 'Nouveau projet', roles: [UserRole.ROLE_USER]}, canActivate: [ACLGuard]},
  {path: 'projects/:id', component: ProjectDetailsComponent, data: {title: 'Détails projet'}},
  {path: 'projects/:id/edit', component: ProjectEditComponent, data: {title: 'Édition projet', roles: [UserRole.ROLE_USER]}, canActivate: [ACLGuard]},
  {path: 'login/cas', component: LoginCasComponent, data: {title: 'Connexion CAS'}},
  {
    path: 'admin',
    loadChildren: () => import('./admin-module/admin-module.module').then(m => m.AdminModuleModule),
    canActivate: [ACLGuard],
    data: {roles: [UserRole.ROLE_ADMIN]}
  },
  {
    path: 'legal-notice',
    component: LegalComponent,
  },
  {
    path: '403',
    component: AccessForbiddenComponent
  },
  { path: 'profile', loadChildren: () => import('./user/user.module').then(m => m.UserModule), canActivate: [ACLGuard], data: {roles: [UserRole.ROLE_USER]} },
  {path: '**', component: PageNotFoundComponent, data: {title: 'Page inconnue'}},
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {

}

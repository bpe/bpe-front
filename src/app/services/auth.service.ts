import {EventEmitter, Injectable, Output} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {catchError, map, shareReplay, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {User} from "../models/user";
import {ToastService} from "./toast.service";
import {UserRole} from "../models/enums/user-role.enum";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  @Output() logoutEvent: EventEmitter<boolean> = new EventEmitter();
  @Output() loginEvent: EventEmitter<boolean> = new EventEmitter();
  @Output() loadConstants: EventEmitter<boolean> = new EventEmitter<boolean>();
  private authenticatedUser?: Observable<User | null>;
  private accessToken: string | null;

  constructor(private httpClient: HttpClient, private toastService: ToastService) {
    this.accessToken = localStorage.getItem('access_token');
  }

  static decodeToken(token: string) {
    try {
      if (!token) {
        return null;
      }
      return JSON.parse(atob(token.split('.')[1]));
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  login(casTicket: string, redirectParam?: string) {
    return this.httpClient.post('/auth/login/cas', {ticket: casTicket, redirect: redirectParam})
      .pipe(
        tap(res => {
          this.saveTokens(res);
          this.loginEvent.emit(true);
          this.getConnectedUser(true).subscribe(_ => null);
        })
      );
  }

  refreshAccessToken() {
    return this.httpClient.post('/auth/refresh', {refresh_token: this.getRefreshToken()})
      .pipe(
        tap(res => {
          this.saveTokens(res);
        }),
      );
  }

  getAccessToken() {
    if (this.accessToken && this.accessToken.length > 0) {
      return this.accessToken;
    }
    return localStorage.getItem('access_token') || '';
  }

  getRefreshToken() {
    return localStorage.getItem('refresh_token');
  }

  setRefreshToken(token: string) {
    localStorage.setItem('refresh_token', token);
  }

  logout() {
    this.logoutEvent.emit(true);

    const refreshToken = this.getRefreshToken();
    if (!this.refreshTokenHasExpired()) {
      this.httpClient.delete<{ redirect: string }>(`/auth/tokens/${refreshToken}`)
        .pipe(catchError(_ => of(null)))
        .subscribe(res => {
          if (res && res.redirect) {
            window.location.href = res.redirect;
          }
        });
    }
    this.authenticatedUser = undefined;
    this.accessToken = null;
    this.removeTokens();
    localStorage.removeItem('userData');
  }

  isLoggedIn() {
    const token = localStorage.getItem('refresh_token');
    const accessToken = localStorage.getItem('access_token');
    return token && token.length > 0 && accessToken && accessToken.length > 0 && !this.refreshTokenHasExpired();
  }

  getConnectedUser(force = false): Observable<User | null> {
    if (this.isLoggedIn()) {
      if (this.authenticatedUser && !force) {
        return this.authenticatedUser;
      }
      const decodedToken = AuthService.decodeToken(this.getAccessToken());
      if (!decodedToken) {
        return of(null);
      }
      this.authenticatedUser = this.httpClient.post<User>("/auth/me", null)
        .pipe(
          map(u => {
            u.role = UserRole[u.role as unknown as keyof typeof UserRole];
            return u;
          }),
          shareReplay()
        );
      return this.authenticatedUser;
    }
    return of(null);
  }

  accessTokenHasExpired() {
    if (!this.getAccessToken()) {
      return false;
    }
    return new Date((AuthService.decodeToken(this.getAccessToken()).exp - 30) * 1000) < new Date(Date.now());
  }

  getRefreshTokenExpiration() {
    return localStorage.getItem('expiration');
  }

  getOldUsers(): string[] {
    const oldUsers = localStorage.getItem('oldUsers');
    if (oldUsers) {
      return JSON.parse(oldUsers);
    }
    return [];
  }

  addOldUser(username: string) {
    const oldUsers = this.getOldUsers();
    if (!oldUsers.includes(username)) {
      localStorage.setItem('oldUsers', JSON.stringify([...oldUsers, username]));
    }
  }

  removeOldUser(username: string) {
    const oldUsers = this.getOldUsers();
    const removeIdx = oldUsers.indexOf(username);
    if (removeIdx >= 0) {
      oldUsers.splice(removeIdx, 1);
      localStorage.setItem('oldUsers', JSON.stringify(oldUsers));
    }
  }

  saveTokens(authenticationResult: any) {
    if (authenticationResult.refresh_token) {
      localStorage.setItem('refresh_token', authenticationResult.refresh_token);
      localStorage.setItem('expiration', authenticationResult.expiration);
    }
    this.accessToken = authenticationResult.access_token;
    localStorage.setItem('access_token', authenticationResult.access_token);
  }

  private removeTokens() {
    localStorage.removeItem('refresh_token');
    this.accessToken = null;
    localStorage.removeItem('access_token');
    localStorage.removeItem('expiration');
  }

  private refreshTokenHasExpired() {
    const expiration = localStorage.getItem('expiration');
    if (!this.getRefreshToken() || !expiration || expiration.length < 1) {
      return true;
    }
    return new Date(expiration) < new Date(Date.now());
  }

  saveData(data: any, key: string) {
    const userData = JSON.parse(localStorage.getItem('userData') || '{}');
    userData[key] = data;
    localStorage.setItem('userData', JSON.stringify(userData));
  }

  getData(key: string) {
    const userData = JSON.parse(localStorage.getItem('userData') || '{}');
    return userData[key];
  }

  goToLogin(redirectUrl: string = '/') {
    this.httpClient.get<{ redirect: string }>(`/auth/login/cas`).pipe(catchError(
      err => {
        const body = err.error;
        if (err.status == 401 && body) {
          return of(body);
        }
        return of(null);
      }
    ))
      .subscribe(res => {
        const redirectItems = redirectUrl.split('/').join(',');
        if (res && res.redirect) {
          window.location.href = `${res.redirect}?redirect=${redirectItems}`;
        } else {
          this.toastService.error("", "Erreur lors de récupération de l'URL de connexion");
        }
      });
  }
}


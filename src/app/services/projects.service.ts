import {Injectable} from '@angular/core';
import {Project} from "../models/project";
import {defaultIfEmpty, forkJoin, map, mergeMap, Observable, shareReplay, switchMap} from "rxjs";
import { HttpClient } from "@angular/common/http";
import {Session} from "../models/session";
import {AppConfigService} from "./app-config.service";
import {ProjectAttachment} from "../models/project-attachment";
import {ProjectComment} from "../models/project-comment";
import {ProjectStatus} from "../models/project-status";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  private projectsCache: Map<number, Observable<Project>> = new Map();
  private sessions?: Observable<Session[]>;


  constructor(private http: HttpClient) {}

  getSessions(): Observable<Session[]> {
    if (!this.sessions) {
      this.sessions = this.http.get<Session[]>("/sessions").pipe(
        map(sessions => sessions.map(s => {
          s.timelineSteps.forEach(s => {
            // End date is s.end minus 1 minute
            s.end = new Date(s.end);
            s.end = new Date(s.end.getFullYear(), s.end.getMonth(), s.end.getDate() - 1, 23, 59);
            s.start = new Date(s.start)
          })
          return s;
        })),
        shareReplay(),
      )
    }
    return this.sessions;
  }

  getProject(id: number, force = false): Observable<Project> {
    if (this.projectsCache.has(id) && !force) {
      return this.projectsCache.get(id) as Observable<Project>;
    }

    const project$ = this.http.get<Project>(`/projects/${id}`).pipe(
      map(project => new Project(project)),
      // Add attachments
      switchMap(project =>
        this.getAttachmentsForProject(project).pipe(map(attachments => {
          project.attachments = attachments;
          return project;
        }))
      ),
      // Add comments
      switchMap(project => this.getCommentsForProject(project).pipe(map(comments => {
          project.comments = comments;
          return project;
        }))
      ),
      shareReplay(),
    );

    this.projectsCache.set(id, project$);
    return project$;

  }

  getSession(id: number | string) {
    return this.getSessions().pipe(
      map(sessions => sessions.find(s => s.id === id || s.slug === id)),
      map(session => {
          if (!session) {
            throw Error(`Session ${id} not found`)
          }
          return session;
        }
      ))
  }

  getProjectsForCurrentSession() {
    return this.getCurrentSession().pipe(switchMap(s => this.getProjectsForSession(s)))
  }

  getCurrentSession() {
    return this.getSession(AppConfigService.settings.currentSessionId);
  }

  getAttachmentsForProject(p: Project) {
    return this.http.get<ProjectAttachment[]>(`/projects/${p.id}/attachments`).pipe(map(
      attachments => attachments.map(a => {
        a.resourcePath = AppConfigService.settings.apiURL + a.resourcePath;
        return a;
      })
    ));
  }

  getCommentsForProject(p: Project) {
    return this.http.get<ProjectComment[]>(`/projects/${p.id}/comments`);
  }

  getProjectsForSession(s: Session, full = false) {
    return this.http.get<Project[]>(`/sessions/${s.id}/projects`, {params: {full}}).pipe(
      map(projects => projects.map(p => {
        const project = new Project(p)
        project.session = s.id;
        return project;
      })),

      // Get attachments for each project and add them to the project (/projects/:id/attachments)
      mergeMap(projects => forkJoin(projects.map(
        p => this.getAttachmentsForProject(p).pipe(map(attachments => {
          p.attachments = attachments;
          return p;
        }))
      )).pipe(defaultIfEmpty([])), 5)
    );
  }

  getProjectCategories(): Observable<{ id: number, name: string }[]> {
    return this.http.get<{
      id: number,
      name: string
    }[]>("/projects/categories").pipe(map(categories => categories.sort((a, b) => a.name.localeCompare(b.name))));
  }

  createProject(project: Project) {
    return this.http.post<Project>("/projects", {
      title: project.title,
      description: project.description,
      details: project.details,
      category: project.category.id,
      budget: project.budget,
      owner: project.owner.id,
      phone: project.phone,
      workgroup: project.workgroup,
      session: project.session,
    }).pipe(map(p => new Project(p)));
  }

  updateProject(project: Project) {
    if (!project.id) throw Error("Missing project id");
    this.projectsCache.delete(project.id);
    return this.http.patch<Project>(`/projects/${project.id}`, {
      title: project.title,
      description: project.description,
      details: project.details,
      category: project.category.id,
      budget: project.budget,
      owner: project.owner.id,
      phone: project.phone,
      workgroup: project.workgroup,
      session: project.session,
    });
  }

  addAttachment(projectId: number, file: File, attachmentType: string, description: string) {
    const formData = new FormData();
    formData.append("attachment", file);
    formData.append("type", attachmentType);
    formData.append("description", description);
    return this.http.post(`/projects/${projectId}/attachments`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  deleteProject(projectId: number) {
    return this.http.delete(`/projects/${projectId}`);
  }

  deleteAttachment(attachmentId: string) {
    return this.http.delete(`/attachments/${attachmentId}`);
  }

  updateProjectStatus(id: number, status: ProjectStatus) {
    return this.http.patch(`/projects/${id}`, {status});
  }

  updateAllocatedBudget(id: number, allocatedBudget: number) {
    return this.http.patch(`/projects/${id}`, {allocatedBudget});
  }

  updateProjectRealised(id: number, realised: boolean) {
    return this.http.patch(`/projects/${id}`, {realised});
  }

  getProjectsForUser(user: User) {
    return this.http.get<Project[]>(`/projects/`, {params: {user: user.id}}).pipe(
      map(projects => projects.map(p => new Project(p))),
      // Get attachments for each project and add them to the project (/projects/:id/attachments)
      mergeMap(projects => forkJoin(projects.map(
        p => this.getAttachmentsForProject(p).pipe(map(attachments => {
          p.attachments = attachments;
          return p;
        }))
      )).pipe(defaultIfEmpty([])), 5),
      // Get comments for each project and add them to the project (/projects/:id/comments)
      mergeMap(projects => forkJoin(projects.map(
        p => this.getCommentsForProject(p).pipe(map(comments => {
          p.comments = comments;
          return p;
        })))))
    );
  }

  previewResultMail(projectId: number) {
    // Return the mail as a string, avoid json parsing
    return this.http.post(`/projects/${projectId}/sendresultmail?dryrun=true`, {}, {
      responseType: 'text',
      headers: {'Accept': 'text/plain'}
    });
  }

  sendResultMail(projectId: number) {
    return this.http.post(`/projects/${projectId}/sendresultmail`, {});
  }

  addComment(id: number, newComment: ProjectComment) {
    return this.http.post(`/projects/${id}/comments`, {
      content: newComment.content,
      published: newComment.published
    });
  }

  updateComment(id: number, comment: ProjectComment) {
    return this.http.patch(`/projects/${id}/comments/${comment.id}`, {
      published: comment.published
    });
  }

  /**
   * Get a project without attachments, get from the API, not from the cache
   * @param id
   */
  getProjectWithoutAttachments(id: number) {
    return this.http.get<Project>(`/projects/${id}`).pipe(map(p => new Project(p)));
  }

  removeFromCache(id: number) {
    this.projectsCache.delete(id);
  }
}

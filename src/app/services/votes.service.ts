import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {shareReplay, tap} from "rxjs/operators";
import {ProjectsService} from "./projects.service";
import {Project} from "../models/project";

@Injectable({
  providedIn: 'root'
})
export class VotesService {
  private userVotedForProject$: Subject<number> = new Subject<number>();
  newVoteCountForProject$: Subject<Project> = new Subject<Project>();
  private userVotesBySessionMap: Map<number, Observable<number[]>> = new Map<number, Observable<number[]>>();

  constructor(
    private http: HttpClient,
    private projectService: ProjectsService,
  ) {
    this.userVotedForProject$.subscribe((projectId) => {
      this.projectService.removeFromCache(projectId);
      this.projectService.getProjectWithoutAttachments(projectId).subscribe({
        next: project => {
          this.userVotesBySessionMap.delete(project.session)
          this.newVoteCountForProject$.next(project);
        }
      })
    })
  }

  getVotesForUserAndSession(sessionId: number, userId: number, force = false): Observable<number[]> {
    if (!this.userVotesBySessionMap.has(sessionId) || force) {
      this.userVotesBySessionMap.set(sessionId, this.http.get<number[]>(`/sessions/${sessionId}/votes/${userId}`).pipe(
        shareReplay()
      ));
    }
    return this.userVotesBySessionMap.get(sessionId) as Observable<number[]>;
  }

  getVotesForCurrentSession(userId: number, force: boolean) {
    this.projectService.getCurrentSession().subscribe(session => {
      if (!session) {
        return;
      }
      this.getVotesForUserAndSession(session.id, userId, force).subscribe();
    });
  }

  voteForProject(projectId: number): Observable<null> {
    return this.http.post<null>(`/projects/${projectId}/vote`, {}).pipe(tap(() => {
      this.userVotedForProject$.next(projectId);
    }));
  }

  unvoteForProject(projectId: number): Observable<null> {
    return this.http.delete<null>(`/projects/${projectId}/vote`).pipe(tap(() => {
      this.userVotedForProject$.next(projectId);
    }));
  }

}

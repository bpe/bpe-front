import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {UserRole} from "../models/enums/user-role.enum";

@Injectable({
  providedIn: 'root'
})
export class AclService {

  constructor(private authService: AuthService) {
  }

  /**
   * Check if connected user has at least one role in roles
   *
   * @param roles list of roles to check
   * @param redirectOnMissingUser redirect to use as parameter to goToLogin if user is not connected
   */
  hasRole(roles: (UserRole | '*')[], redirectOnMissingUser: string|null = '/'): Observable<boolean> {
    if (roles.includes('*')) {
      return of(true);
    }
    return this.authService.getConnectedUser().pipe(
      map((u) => {
        if (!u) {
          // Redirect to login page
          if (redirectOnMissingUser && redirectOnMissingUser.length > 0) {
            this.authService.goToLogin(redirectOnMissingUser);
          }
          return false;
        }

        if (u.role === UserRole.ROLE_ADMIN) {
          return true;
        }


        if (!u.role || u.role.length === 0) {
          return false;
        }
        let hasRole = false;

        roles.forEach(r => {
          if (r != '*' && u.role === r) {
            hasRole = true;
          }
        });
        return hasRole;
      })
    );
  }
}

import {Injectable} from '@angular/core';
import { HttpBackend, HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, merge, mergeMap, tap} from "rxjs/operators";
import {Observable, of, throwError} from "rxjs";
import {ToastService} from "./toast.service";

export interface AppConfig {
  env: string;
  apiURL: string;
  version: string;
  currentSessionId: number;
}

/**
 * Service to set API entry point from a JSON config file.
 */
@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  static settings: AppConfig;

  private httpClient: HttpClient;

  constructor(handler: HttpBackend, private toastService:ToastService) {
    this.httpClient = new HttpClient(handler);
  }

  /**
   * Seed AppConfigService using json config file (set in environment)
   */
  load() {
    const jsonFile = environment.configFile;

    let config$ = this.httpClient.get(jsonFile);
    if (!environment.production) {
      config$ = config$.pipe(
        tap({
          next: res => {
            AppConfigService.settings = (res as AppConfig);
            AppConfigService.settings.version = environment.appVersion;
            console.log(AppConfigService.settings);
          }
        }));
    } else {
      config$ = config$.pipe(
        tap((res: any) => {
          AppConfigService.settings = {
            env: "prod",
            apiURL: 'https://' + window.location.host + '/api',
            version: environment.appVersion,
            currentSessionId: res.currentSessionId
          };
        }),
      )
    }
    return config$.pipe(
      mergeMap(_ => this.getSessionId().pipe(
        tap(id => AppConfigService.settings.currentSessionId = id),
        catchError(_ => {
          AppConfigService.settings.currentSessionId =0;
          this.toastService.error("", "Impossible de récupérer l'identifiant de session active");
          return of(null)
        })
      )),
      catchError(_ => throwError((_: any) => new Error("Failed to load config file"))))
  }

  private getSessionId(): Observable<number> {
    return this.httpClient.get<number>(AppConfigService.settings.apiURL + "/settings/active-session-id");
  }
}

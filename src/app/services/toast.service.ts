import {Injectable, TemplateRef} from '@angular/core';

/**
 * Service to manage a toasts list.
 */
@Injectable({
  providedIn: 'root'
})
export class ToastService {
  /**
   * List of active toasts to display.
   */
  toasts: any[] = [];

  /**
   * Default toast configuration. Contains the minimal configuration.
   *
   * @private
   */
  private globalOptions = {
    className: 'bg-light text-dark',
    autohide: true,
    delay: 5000,
    icon: ''
  };

  constructor() {
  }

  /**
   * Create a success toast
   *
   * @param title Toast title (can be null)
   * @param content Toast content as a string or a TemplateRef.
   */
  success(title: string, content: string | TemplateRef<any>) {
    const options = {...this.globalOptions};
    options.className = 'bg-success text-light';
    options.icon = 'bi-check-lg';
    const toast = {title, content, options}
    this.toasts.push(toast);
    return toast;
  }

  /**
   * Create a warning toast
   *
   * @param title Toast title (can be null)
   * @param content Toast content as a string or a TemplateRef.
   */
  warning(title: string, content: string | TemplateRef<any>) {
    const options = {...this.globalOptions};
    options.className = 'bg-warning text-dark';
    options.icon = 'bi-exclamation-lg';
    const toast = {title, content, options}
    this.toasts.push(toast);
    return toast;
  }

  /**
   * Create an error toast
   *
   * @param title Toast title (can be null)
   * @param content Toast content as a string or a TemplateRef.
   */
  error(title: string, content: string | TemplateRef<any>) {
    const options = {...this.globalOptions};
    options.className = 'bg-danger text-light';
    options.icon = 'bi-exclamation-triangle-fill';
    const toast = {title, content, options}
    this.toasts.push(toast);
    return toast;
  }

  /**
   * Create an info toast
   *
   * @param title Toast title (can be null)
   * @param content Toast content as a string or a TemplateRef.
   * @param autohide Control autohide toast parameter
   */
  info(title: string, content: string | TemplateRef<any>, autohide = true) {
    const options = {...this.globalOptions};
    options.className = 'bg-secondary text-white';
    options.icon = 'bi-info-circle-fill';
    options.autohide = autohide;
    const toast = {title, content, options}
    this.toasts.push(toast);
    return toast;
  }

  /**
   * Removes a toast from the service toasts list.
   */
  remove(toast: any) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}

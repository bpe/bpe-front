import {Component, OnInit} from '@angular/core';
import * as Aos from 'aos';
import {fadeAnimation} from "./animations";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent implements OnInit{
  title = 'BPE UTC';

  ngOnInit(): void {
    Aos.init();
    Aos.refresh();
  }
}

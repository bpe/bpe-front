import {Component, OnInit} from '@angular/core';
import {User} from "../models/user";
import {AuthService} from "../services/auth.service";
import {Project} from "../models/project";
import {ProjectsService} from "../services/projects.service";
import {Session} from "../models/session";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  authenticatedUser!: User;
  sessionsProjectsMap: Map<Session, Project[]> = new Map();
  sortedSessions: Session[] = [];

  constructor(private authService: AuthService, private projectsService: ProjectsService) {

  }

  ngOnInit() {
    this.authService.getConnectedUser().subscribe({
      next: (user) => {
        if (user) {
          this.authenticatedUser = user;
          forkJoin([this.projectsService.getSessions(), this.projectsService.getProjectsForUser(user)]).subscribe({
            next: ([sessions, projects]) => {
              sessions.forEach(session => {
                // Find projects for session
                const projectsForSession = projects.filter(project => project.session === session.id);
                if (projectsForSession.length > 0) {
                  this.sessionsProjectsMap.set(session, projectsForSession);
                }
              });

              // Sort sessions by date (most recent first)
              this.sortedSessions = Array.from(this.sessionsProjectsMap.keys()).sort((a, b) => {
                // Get DEPOSIT start date for each session
                const aDate = a.timelineSteps.find(step => step.timelineItem.id === 'DEPOSIT')?.start;
                const bDate = b.timelineSteps.find(step => step.timelineItem.id === 'DEPOSIT')?.start;
                return bDate?.getTime()! - aDate?.getTime()!;
              });
            }
          });
        }
      }
    });
  }


  canEditProject(project: Project, session: Session) {
    // Can edit if session is in "DEPOSIT" timeline step or project is in ACTION_NEEDED status and timeline step is in "PROJECT_EXAMNATION" timeline step
    const activeTimelineStep = session.timelineSteps.find(step => step.start.getTime() <= Date.now() && step.end.getTime() >= Date.now());
    if (activeTimelineStep) {
      if (activeTimelineStep.timelineItem.id === 'DEPOSIT') {
        return true;
      } else if (project.status === 'ACTION_NEEDED' && activeTimelineStep.timelineItem.id === 'PROJECT_EXAMINATION') {
        return true;
      }
    }
    return false;
  }
}

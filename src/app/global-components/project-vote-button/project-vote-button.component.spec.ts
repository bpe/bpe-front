import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectVoteButtonComponent } from './project-vote-button.component';

describe('ProjectVoteButtonComponent', () => {
  let component: ProjectVoteButtonComponent;
  let fixture: ComponentFixture<ProjectVoteButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectVoteButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectVoteButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

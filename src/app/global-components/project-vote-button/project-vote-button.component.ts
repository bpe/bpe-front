import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {Project} from "../../models/project";
import {AuthService} from "../../services/auth.service";
import {VotesService} from "../../services/votes.service";
import {ToastService} from "../../services/toast.service";
import {delay} from "rxjs";
import {User} from "../../models/user";
import {NgbOffcanvas} from "@ng-bootstrap/ng-bootstrap";
import {UserVotesModalComponent} from "../user-votes-modal/user-votes-modal.component";

@Component({
  selector: 'app-project-vote-button',
  templateUrl: './project-vote-button.component.html',
  styleUrls: ['./project-vote-button.component.scss']
})
export class ProjectVoteButtonComponent implements OnInit {
  @ViewChild('moreThan3Votes') public moreThan3VotesTmpl!: TemplateRef<any>;
  @Input() project!: Project;
  @Output() userHasVoted = false;
  @Output() userCanVote = false;
  @Output() voteChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  isLoading = false;
  private authenticatedUser?: User | null;

  constructor(
    private authService: AuthService,
    private voteService: VotesService,
    private toastService: ToastService,
    private offcanvasService: NgbOffcanvas
  ) {
  }

  ngOnInit(): void {
    if (!this.project || !this.project.id) {
      throw new Error('ProjectVoteButtonComponent: project is required');
    }
    this.voteService.newVoteCountForProject$.subscribe({
      next: (project) => {
        if (this.project && project.id === this.project.id) {
          this.init();
        }
      }
    });
    setTimeout(() => {
      this.init();
    }, 0);
  }

  init() {
    this.authService.getConnectedUser().subscribe(user => {
      this.authenticatedUser = user;
      if (!user) {
        this.userCanVote = false;
        return;
      }
      this.userCanVote = ['utc-etu', 'utc-etu3c'].includes(user?.profile);
      if (!this.userCanVote) {
        return;
      }
      this.voteService.getVotesForUserAndSession(this.project.session, user.id).subscribe(
        {
          next: votes => {
            this.userHasVoted = votes.some(id => id === this.project.id);
          },
          error: error => {
            console.error(error);
            this.toastService.error('', 'Une erreur est survenue lors de la récupération des votes');
          }
        });
    })
  }

  toggleVote() {
    if (!this.userCanVote) {
      return;
    }
    this.isLoading = true;
    if (this.userHasVoted) {
      this.voteService.unvoteForProject(this.project.id)
        .pipe(delay(250))
        .subscribe({
          next: () => {
            this.userHasVoted = false;
            this.toastService.success('', 'Vote retiré');
            this.isLoading = false;
            this.voteChanged.emit(true);
          },
          error: error => {
            console.error(error);
            this.toastService.error('', 'Une erreur est survenue lors de la suppression du vote');
            this.isLoading = false;
          }
        })
    } else {
      this.voteService.getVotesForUserAndSession(this.project.session, this.authenticatedUser?.id as number).subscribe({
        next: votes => {
          if (votes.length >= 3) {
            this.toastService.warning('', this.moreThan3VotesTmpl);
            this.isLoading = false;
            return;
          } else {
            this.voteService.voteForProject(this.project.id)
              .pipe(delay(250))
              .subscribe({
                next: () => {
                  this.userHasVoted = true;
                  this.toastService.success('', 'Vote enregistré');
                  this.isLoading = false;
                  this.voteChanged.emit(true);
                },
                error: error => {
                  console.error(error);
                  this.toastService.error('', error.error || 'Une erreur est survenue lors de l\'enregistrement du vote');
                  this.isLoading = false;
                }
              })
          }
        }
      });
    }
  }

  openVotesPanel() {
    this.offcanvasService.open(UserVotesModalComponent, {
      ariaLabelledBy: 'user votes offcanvas',
      position: "bottom",
      container: 'body',
      panelClass: 'h-auto'
    });
  }
}

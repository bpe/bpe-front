import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusConverter'
})
export class StatusConverterPipe implements PipeTransform {
  private statusMap = new Map([
    ['ACCEPTED', 'Validé'],
    ['PENDING_VALIDATION', 'En attente de validation'],
    ['NOT_ACCEPTED', 'Refusé'],
    ['ACTION_NEEDED', 'Action requise'],
    ['CANCELED', 'Annulé']
  ])
  transform(value: string, ...args: unknown[]): string {
    if (this.statusMap.has(value)) {
      return this.statusMap.get(value) as string
    }
    return value
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusColor'
})
export class StatusColorPipe implements PipeTransform {
  private statusColor = new Map([
    ['ACCEPTED', 'success'],
    ['PENDING_VALIDATION', 'info'],
    ['NOT_ACCEPTED', 'danger'],
    ['ACTION_NEEDED', 'warning'],
    ['CANCELED', 'muted']
  ])
  transform(value: string, ...args: unknown[]): string {
    if (this.statusColor.has(value)) {
      return this.statusColor.get(value) as string
    }
    return value;
  }

}

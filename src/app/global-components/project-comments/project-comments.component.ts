import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project} from "../../models/project";
import {UserRole} from "../../models/enums/user-role.enum";
import {ProjectComment} from "../../models/project-comment";
import {ProjectsService} from "../../services/projects.service";
import {ToastService} from "../../services/toast.service";

@Component({
  selector: 'app-project-comments',
  templateUrl: './project-comments.component.html',
  styleUrls: ['./project-comments.component.scss']
})
export class ProjectCommentsComponent implements OnInit {
  @Input() project!: Project;
  @Output() projectUpdated = new EventEmitter<Boolean>();

  protected readonly UserRole = UserRole;
  newComment!: ProjectComment;

  constructor(
    private projectsService: ProjectsService,
    private toastService: ToastService,
  ) {
  }

  ngOnInit(): void {
    this.seedNewComment();
  }

  addComment() {
    this.projectsService.addComment(this.project.id, this.newComment).subscribe({
      next: () => {
        this.seedNewComment();
        this.toastService.success("", "Commentaire ajouté");
        this.projectsService.getProject(this.project.id, true).subscribe({
          next: (project: Project) => {
            this.project = project;
          }
        });
        this.projectUpdated.emit(true);
      },
      error: (err: any) => {
        console.error(err);
        this.toastService.error("", "Erreur lors de l'ajout du commentaire");
      }
    });
  }

  togglePublishedForComment(comment: ProjectComment) {
    comment.published = !comment.published;
    this.projectsService.updateComment(this.project.id, comment).subscribe({
      next: () => {
        this.toastService.success("", "Commentaire mis à jour");
        this.projectsService.getProject(this.project.id, true).subscribe({
          next: (project: Project) => {
            this.project = project;
          }
        });
        this.projectUpdated.emit(true);
      }
    });
  }

  private seedNewComment() {
    this.newComment = {content: "", date: new Date(), user: "Moi", published: true, id: -1};
  }
}

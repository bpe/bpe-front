import {Component, Input, TemplateRef} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './delete-confirm-modal.component.html',
  styleUrls: ['./delete-confirm-modal.component.css']
})
export class DeleteConfirmModalComponent {

  @Input()title = 'Confirmation';
  @Input()reversible = false;
  @Input()message: TemplateRef<any> | string = 'Confirmez-vous l\'action ?';
  @Input()templateContext: any;

  constructor(public modal: NgbActiveModal) {}

  isTemplate(obj: any) {
    return obj instanceof TemplateRef;
  }

  getTemplate(message: TemplateRef<any> | string) {
    if(!this.isTemplate(message)) {
      return null;
    }
    return message as TemplateRef<any>;
  }
}

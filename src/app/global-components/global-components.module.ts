import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {GlassBgComponent} from "./glass-bg/glass-bg.component";
import {ProjectDetailsComponent} from "../components/projects/project-details/project-details.component";
import {ProjectsCarrouselComponent} from "../components/projects/projects-carrousel/projects-carrousel.component";
import {ProjectCardComponent} from "../components/projects/project-card/project-card.component";
import {ProjectsGalleryComponent} from "../components/projects/projects-gallery/projects-gallery.component";
import {SessionCalendarCardComponent} from "../components/session-calendar-card/session-calendar-card.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterLink} from "@angular/router";
import {DateinputconverterDirective} from './dateinputconverter.directive';
import {DeleteConfirmModalComponent} from "./delete-confirm-modal/delete-confirm-modal.component";
import {NgbTooltip} from "@ng-bootstrap/ng-bootstrap";
import { ProjectEditComponent } from './project-edit/project-edit.component';
import {QuillEditorComponent} from "ngx-quill";
import {DropfileDirective} from "../directives/dropfile.directive";
import { RecoverDraftModalComponent } from './project-edit/recover-draft-modal/recover-draft-modal.component';
import { StatusConverterPipe } from './pipes/status-converter.pipe';
import { StatusColorPipe } from './pipes/status-color.pipe';
import { ProjectCommentsComponent } from './project-comments/project-comments.component';
import { ResultMailSenderComponent } from './result-mail-sender/result-mail-sender.component';
import {HasRoleDirective} from "../directives/has-role.directive";
import { ProjectVoteButtonComponent } from './project-vote-button/project-vote-button.component';
import { UserVotesModalComponent } from './user-votes-modal/user-votes-modal.component';



@NgModule({
  declarations: [
    GlassBgComponent,
    ProjectDetailsComponent,
    ProjectsCarrouselComponent,
    ProjectCardComponent,
    ProjectsGalleryComponent,
    SessionCalendarCardComponent,
    DateinputconverterDirective,
    DeleteConfirmModalComponent,
    ProjectEditComponent,
    DropfileDirective,
    RecoverDraftModalComponent,
    StatusConverterPipe,
    StatusColorPipe,
    ProjectCommentsComponent,
    ResultMailSenderComponent,
    HasRoleDirective,
    ProjectVoteButtonComponent,
    UserVotesModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterLink,
    NgbTooltip,
    ReactiveFormsModule,
    QuillEditorComponent,
    NgOptimizedImage,
  ],
  exports: [
    GlassBgComponent,
    ProjectDetailsComponent,
    ProjectsCarrouselComponent,
    SessionCalendarCardComponent,
    DateinputconverterDirective,
    DeleteConfirmModalComponent,
    DropfileDirective,
    ProjectCardComponent,
    StatusConverterPipe,
    StatusColorPipe,
    ProjectCommentsComponent,
    HasRoleDirective
  ]
})
export class GlobalComponentsModule { }

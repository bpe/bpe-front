import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {VotesService} from "../../services/votes.service";
import {ProjectsService} from "../../services/projects.service";
import {Session} from "../../models/session";
import {Project} from "../../models/project";
import {NgbActiveOffcanvas} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-user-votes-modal',
  templateUrl: './user-votes-modal.component.html',
  styleUrls: ['./user-votes-modal.component.scss']
})
export class UserVotesModalComponent implements OnInit {
  votes: number[] = [];
  session!: Session;
  myProjects: Project[] = [];

  constructor(
    private authService: AuthService,
    private voteService: VotesService,
    private projectService: ProjectsService,
    private ngbActiveOffcanvas: NgbActiveOffcanvas
  ) {
  }

  ngOnInit(): void {
    this.authService.getConnectedUser().subscribe(user => {
      if (!user) {
        return;
      }
      this.projectService.getCurrentSession().subscribe(session => {
        this.session = session;
        this.voteService.getVotesForUserAndSession(session?.id, user.id).subscribe(votes => {
          this.votes = votes;
          this.projectService.getProjectsForCurrentSession().subscribe(projects => {
            this.myProjects = projects.filter(project => this.votes.includes(project.id));
          });
        })
      });
    })
  }

  closeOffCanvas() {
    this.ngbActiveOffcanvas.dismiss();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserVotesModalComponent } from './user-votes-modal.component';

describe('UserVotesModalComponent', () => {
  let component: UserVotesModalComponent;
  let fixture: ComponentFixture<UserVotesModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserVotesModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserVotesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

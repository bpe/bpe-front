import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultMailSenderComponent } from './result-mail-sender.component';

describe('ResultmailsenderComponent', () => {
  let component: ResultMailSenderComponent;
  let fixture: ComponentFixture<ResultMailSenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultMailSenderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultMailSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

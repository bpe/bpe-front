import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Project} from "../../models/project";
import {ProjectsService} from "../../services/projects.service";
import {ToastService} from "../../services/toast.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-result-mail-sender',
  templateUrl: './result-mail-sender.component.html',
  styleUrls: ['./result-mail-sender.component.scss']
})
export class ResultMailSenderComponent implements OnChanges, OnInit{
  @Input() project!: Project;
  preview: any;
  error?: string;

  constructor(
    private projectsService: ProjectsService,
    private toastService: ToastService,
    private ngbActiveModal: NgbActiveModal,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
    this.loadPreview();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["project"] && changes["project"].currentValue) {
      this.project = changes["project"].currentValue;
      this.loadPreview();
    }
  }

  loadPreview() {
    this.projectsService.previewResultMail(this.project.id).subscribe({
      next: (preview: string) => {
        this.preview = this.sanitizer.bypassSecurityTrustHtml(preview);
      },
      error: err => {
        this.error = err.error;
        console.error(err);
        this.toastService.error("", "Erreur lors de la génération de l'aperçu");
      }
    });
  }

  sendResultMail() {
    this.projectsService.sendResultMail(this.project.id).subscribe({
      next: () => {
        this.toastService.success('', "Email envoyé");
        this.ngbActiveModal.close();
      },
      error: err => {
        console.error(err);
        this.toastService.error("", "Erreur lors de l'envoi de l'email");
      }
    });
  }

  closeModal(reason: string) {
    this.ngbActiveModal.dismiss(reason)
  }
}

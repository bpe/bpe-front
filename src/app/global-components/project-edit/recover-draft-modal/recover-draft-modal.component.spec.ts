import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoverDraftModalComponent } from './recover-draft-modal.component';

describe('RecoverDraftModalComponent', () => {
  let component: RecoverDraftModalComponent;
  let fixture: ComponentFixture<RecoverDraftModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecoverDraftModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecoverDraftModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-recover-draft-modal',
  templateUrl: './recover-draft-modal.component.html',
  styleUrls: ['./recover-draft-modal.component.scss']
})
export class RecoverDraftModalComponent {
  @Input() timestamp!: Date;

  constructor(protected modal: NgbActiveModal) {
  }

}

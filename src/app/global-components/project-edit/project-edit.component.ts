import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ProjectsService} from "../../services/projects.service";
import {Session} from "../../models/session";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user";
import {AuthService} from "../../services/auth.service";
import {Project} from "../../models/project";
import {ToastService} from "../../services/toast.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {RecoverDraftModalComponent} from "./recover-draft-modal/recover-draft-modal.component";
import { HttpEventType } from "@angular/common/http";
import {concatAll, forkJoin, from, map} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {UserRole} from "../../models/enums/user-role.enum";

function get_file_size_in_mb(file: any) {
  let sizeInBytes: number = file.size;
  return sizeInBytes / (1024 * 1024);
}

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.scss']
})
export class ProjectEditComponent implements OnInit, OnChanges {
  session?: Session;
  projectForm!: FormGroup;
  projectCategories!: { id: number, name: string }[];
  private currentUser!: User | null;
  quillFormats = ['bold', 'strike', 'underline', 'header', 'indent', 'list', 'italic'];
  quillModules = {
    toolbar: [
      [{header: [1, 2, false]}],
      ['bold', 'italic', 'underline'],
      [{'list': 'ordered'}, {'list': 'bullet'}]
    ]
  }

  protected readonly createObjectURL = URL.createObjectURL;
  preview: Map<string, string> = new Map<string, string>();
  attachments: {
    id?: string;
    preview: string;
    file: any;
    name: any;
    type: { id: string; name: string; canEditFileName: boolean }
  }[] = [];
  attachmentTypes = [
    {id: 'IMAGE', name: 'Image', canEditFileName: true},
    {id: 'DESCRIPTION', name: 'Dossier de présentation', canEditFileName: false},
    {id: 'OPTIONAL', name: 'Autre document', canEditFileName: true},
  ];
  isSaving?: string;
  private defaultDetails = "<h1>Contexte</h1><p>Contexte du projet</p><br><br><h1>Objectif</h1><p>Quelle amélioration pour la vie étudiante à l'UTC ?</p><br><br><h1>Actions</h1><p>Actions à mener</p>";
  projectEditCache?: Project;


  constructor(
    private projectsService: ProjectsService,
    private authService: AuthService,
    private toastService: ToastService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.initForm();

    this.activatedRoute.paramMap.subscribe((params) => {
      const projectId = params.get('id');
      if (projectId) {
        this.projectsService.getProject(parseInt(projectId)).subscribe(
          {
            next: (project) => {
              forkJoin([
                this.projectsService.getCurrentSession(),
                this.authService.getConnectedUser(),
                this.projectsService.getProjectCategories()])
                .subscribe(([session, user, categories]) => {
                  this.projectCategories = categories;
                  let canEdit: boolean = this.canEditProject(session, user, project)
                  if (!canEdit) {
                    this.toastService.error("", "Vous n'avez pas les droits pour modifier ce projet.");
                    this.router.navigate(["/"]);
                    return;
                  }

                  this.session = session;
                  this.currentUser = user;
                  this.projectEditCache = project;
                  this.fillFormFromExistingProject(project);
                });
            },
            error: (err) => {
              console.error(err);
              this.toastService.error("", "Impossible de charger le projet.");
              this.router.navigate(["/"]);
            }
          }
        )
      } else {
        // Get current Session and user,
        // If session is not in "DEPOSIT" status, redirect to home
        forkJoin([this.projectsService.getCurrentSession(), this.authService.getConnectedUser()]).subscribe(([session, user]) => {
          let canEdit: boolean = this.canEditProject(session, user)
          if (!canEdit) {
            this.toastService.error("", "Le dépôt de projet n'est pas ouvert actuellement.");
            this.router.navigate(["/"]);
            return;
          }

          this.session = session;
          this.currentUser = user;
          this.projectForm.patchValue({owner: user});
        });

        // Get project categories
        this.projectsService.getProjectCategories().subscribe(categories => {
          this.projectCategories = categories;
          this.projectForm.patchValue({details: this.defaultDetails});

          //check if form values are stored in local storage
          const storedForm = localStorage.getItem("projectForm");
          if (storedForm) {
            const data = JSON.parse(storedForm).data;
            // Check if stored form values are not all null
            const oneNotNull = Object.values(data).some((value) => value !== null);
            // Check if details are not default value
            const detailsNotDefault = data.details !== this.defaultDetails;
            if (oneNotNull && detailsNotDefault) {
              const modal = this.modalService.open(RecoverDraftModalComponent);
              modal.componentInstance.timestamp = JSON.parse(storedForm).timestamp;
              modal.result.then((result) => {
                if (result) {
                  this.projectForm.patchValue(JSON.parse(storedForm).data);
                  this.projectForm.patchValue({budgetFile: null, headerImage: null, attachments: null});
                } else {
                  localStorage.removeItem("projectForm");
                }
              }, (_) => {
                localStorage.removeItem("projectForm");
              });
            }
          }

          // Store form values in local storage
          this.projectForm.valueChanges.subscribe((value) => {
            localStorage.setItem("projectForm", JSON.stringify({data: value, timestamp: Date.now()}));
          });
        });

      }
    });

    this.authService.getConnectedUser().subscribe((user) => {
      if (user?.role === UserRole.ROLE_ADMIN) {
        this.attachmentTypes.push({id: 'ACHIEVEMENT', name: "Réalisation", canEditFileName: true})
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  private initForm() {
    // Form group for project
    this.projectForm = new FormGroup({
      title: new FormControl(null, [Validators.required, Validators.maxLength(60)]),
      description: new FormControl(null, Validators.required),
      details: new FormControl(null, Validators.required),
      headerImage: new FormControl(null),
      category: new FormControl(null, Validators.required),
      budget: new FormControl(null, [Validators.required, Validators.min(1)]),
      budgetFile: new FormControl(null, [Validators.required]),
      workgroup: new FormControl(null),
      // French phone number
      phone: new FormControl(null, [Validators.required, Validators.pattern("^((\\+)33|0)[1-9](\\d{2}){4}$")]),
      attachments: new FormControl(null),
      owner: new FormControl(null, [Validators.required])
    });
  }

  saveProject() {
    if (this.projectForm.valid) {
      this.projectForm.disable();
      this.isSaving = "Enregistrement du projet en cours...";

      if (this.projectEditCache && this.projectEditCache.id) {
        this.updateProject();
      } else {
        this.createProject();
      }

    }
  }

  onFilePicked(event: Event, formKey: string) {
    const maxSize = formKey === "headerImage" ? 5 : 15;

    // Only the 1st file
    let patchItem: { [key: string]: any } = {};
    let files = (event.target as HTMLInputElement).files;
    const file = files ? files[0] : null;

    if (file) {
      const sizeInMb = get_file_size_in_mb(file);
      if (sizeInMb > maxSize) {
        this.toastService.warning(`${file.name} non importé`, `Le fichier doit faire moins de ${maxSize} Mo`);
        return;
      }
    }

    patchItem[formKey] = file;
    this.projectForm.patchValue(patchItem);
    if (file) {
      this.preview.set(formKey, this.createObjectURL(file));
    } else {
      this.preview.delete(formKey);
    }
  }

  headerImageDropped(event: any) {
    event = event[0];
    const sizeInMb = get_file_size_in_mb(event);
    if (sizeInMb > 5) {
      this.toastService.warning(`${event.name} non importé`, "Le fichier doit faire moins de 5 Mo");
      return;
    }
    const filename = event.name;
    // Check if extension is in "png, jpg, jpeg"
    if (!filename.match(/.(jpg|jpeg|png)$/i)) {
      this.toastService.warning("", "Le fichier doit être une image (png, jpg, jpeg)");
      return;
    }
    this.projectForm.patchValue({headerImage: event});
    this.preview.set("headerImage", this.createObjectURL(event));
  }

  isHeaderRatioOk(naturalWidth: number, naturalHeight: number) {
    const max = 500 / 220;
    const min = 240 / 220;
    const ratio = naturalWidth / naturalHeight;
    return ratio >= min && ratio <= max;
  }

  budgetFileDropped(event: any) {
    event = event[0];
    const sizeInMb = get_file_size_in_mb(event);
    if (sizeInMb > 15) {
      this.toastService.warning(`${event.name} non importé`, "Le fichier doit faire moins de 15 Mo");
      return;
    }
    const filename = event.name;
    // Check if extension is PDF
    if (!filename.match(/.(pdf)$/i)) {
      this.toastService.warning("", "Le fichier doit être un document PDF");
      return;
    }
    this.projectForm.patchValue({budgetFile: event});
    this.preview.set("budgetFile", this.createObjectURL(event));
  }

  additionalDocumentsDropped(files: any) {
    // Check for each file if extension is in "pdf, doc, docx, xls, xlsx, odt, ods, jped, jpg, png"
    // Check for each file if size is less than 15 Mo
    for (let file of files) {
      const sizeInMb = get_file_size_in_mb(file);
      if (sizeInMb > 15) {
        this.toastService.warning(`${file.name} non importé`, "Le fichier doit faire moins de 15 Mo");
        continue
      }

      const filename = file.name;
      if (!filename.match(/.(pdf|doc|docx|xls|xlsx|odt|ods|jpeg|jpg|png)$/i)) {
        this.toastService.warning(`${filename} non importé`, "Le fichier doit être un document (pdf, doc, docx, xls, xlsx, odt, ods) ou une image (png, jpg, jpeg)");
      } else {
        const attachmentType = this.determineAttachmentType(file.name);
        let filename = file.name;
        if (!attachmentType.canEditFileName) {
          filename = attachmentType.name;
        }
        this.attachments.push({
          file,
          preview: this.createObjectURL(file),
          name: filename,
          type: attachmentType
        })
      }
    }
  }

  onAttachmentPick(event: Event) {
    let files = (event.target as HTMLInputElement).files;
    if (files) {
      this.additionalDocumentsDropped(files);
    }
  }

  determineAttachmentType(name: string) {
    // Check if is an image
    if (name.match(/.(jpeg|jpg|png)$/i)) {
      return this.attachmentTypes.find((type) => type.id === "IMAGE");
    }
    // Default to optional
    return this.attachmentTypes.find((type) => type.id === "OPTIONAL") as any;
  }

  attachmentTypeSelect(selectedType: any, attachment: {
    preview: string;
    file: any;
    name: any;
    type: { id: string; name: string; canEditFileName: boolean }
  }) {
    if (!selectedType.canEditFileName) {
      attachment.name = selectedType.name;
    } else {
      attachment.name = attachment.file.name;
    }
  }

  removeAttachment(attachment: {
                     preview: string;
                     file: any;
                     name: any;
                     type: { id: string; name: string; canEditFileName: boolean }
                   }
  ) {
    this.attachments = this.attachments.filter((a) => a !== attachment);
  }

  private canEditProject(session: Session, user: User | null, project?: Project) {
    if (!user) {
      return false;
    }

    // Check if user is admin
    if (user.role === UserRole.ROLE_ADMIN) {
      return true;
    }

    // Get active session step
    const activeStep = session.timelineSteps.find((step) => step.start <= new Date() && step.end >= new Date());

    // If no project, session must be in DEPOSIT status
    if (!project && activeStep && activeStep.timelineItem.id === "DEPOSIT") {
      return true;
    }

    // If project, user must be owner of project
    if (!project || !project.owner || project.owner.id !== user.id) {
      return false;
    }

    // To edit a project, session must be in DEPOSIT status or session in PROJECT_EXAMINATION status and project in ACTION_NEEDED status
    if (activeStep && activeStep.timelineItem.id === "DEPOSIT") {
      return true;
    }

    return !!(activeStep && activeStep.timelineItem.id === "PROJECT_EXAMINATION" && project.status === "ACTION_NEEDED");
  }

  private fillFormFromExistingProject(project: Project) {
    this.projectForm.patchValue({
      title: project.title,
      description: project.description,
      details: project.details,
      owner: project.owner,
      headerImage: project.attachments?.find((a) => a.type === "HEADER_IMAGE"),
      category: this.projectCategories.find((c) => c.id === project.category.id),
      budget: project.budget,
      budgetFile: project.attachments?.find((a) => a.type === "BUDGET"),
      workgroup: project.workgroup,
      phone: project.phone,
      attachments: project.attachments?.filter((a) => a.type !== "BUDGET" && a.type !== "HEADER_IMAGE")
    })

    const headerImage = project.attachments?.find((a) => a.type === "HEADER_IMAGE")
    if (headerImage) {
      this.preview.set("headerImage", headerImage.resourcePath);
    }

    const budgetFile = project.attachments?.find((a) => a.type === "BUDGET")
    if (budgetFile) {
      this.preview.set("budgetFile", budgetFile.resourcePath);
    }

    for (let attachment of project.attachments?.filter((a) => a.type !== "BUDGET" && a.type !== "HEADER_IMAGE") || []) {
      this.attachments.push({
        id: attachment.id,
        file: null,
        preview: attachment.resourcePath,
        name: attachment.description,
        type: this.attachmentTypes.find((type) => type.id === attachment.type) || this.attachmentTypes[0]
      })
    }
  }

  private createProject() {
    const project = new Project(this.projectForm.value);
    if (this.session?.id) {
      project.session = this.session.id;
    }
    if (this.currentUser) {
      project.owner = this.currentUser;
    }

    this.projectsService.createProject(project).subscribe((project) => {
      localStorage.removeItem("projectForm");

      const observables = [];

      // Save header Image (HEADER_IMAGE)
      observables.push(
        this.projectsService.addAttachment(project.id, this.projectForm.value.headerImage, "HEADER_IMAGE", `Image en-tête ${project.title}`)
          .pipe(map((event: any) => {
              if (event.type == HttpEventType.UploadProgress) {
                const uploadProgress = Math.round(100 * (event.loaded / event.total));
                return `Enregistrement de l'image d'en tête en cours... (${uploadProgress}%)`;
              }
              if (event.type == HttpEventType.Response) {
                return `Enregistrement de l'image d'en tête terminé !`;
              }
              return `Enregistrement de l'image d'en tête en cours...`;
            }
          ))
      );

      // Save budget file (BUDGET)
      observables.push(
        this.projectsService.addAttachment(project.id, this.projectForm.value['budgetFile'], "BUDGET", `Budget détaillé`)
          .pipe(map((event: any) => {
              if (event.type == HttpEventType.UploadProgress) {
                const uploadProgress = Math.round(100 * (event.loaded / event.total));
                return `Enregistrement du budget détaillé en cours... (${uploadProgress}%)`;
              }
              if (event.type == HttpEventType.Response) {
                return `Enregistrement du budget détaillé terminé !`;
              }
              return `Enregistrement du budget détaillé en cours...`;
            }
          ))
      );

      // Save attachments (DESCRIPTION, OPTIONAL, IMAGE)
      for (let attachment of this.attachments) {
        observables.push(
          this.projectsService.addAttachment(project.id, attachment.file, attachment.type.id, attachment.name).pipe(
            map((event: any) => {
                if (event.type == HttpEventType.UploadProgress) {
                  const uploadProgress = Math.round(100 * (event.loaded / event.total));
                  return `Enregistrement de ${attachment.name} en cours... (${uploadProgress}%)`;
                }
                if (event.type == HttpEventType.Response) {
                  return `Enregistrement de ${attachment.name} terminé !`;
                }
                return `Enregistrement de ${attachment.name} en cours...`;
              }
            ))
        );
      }

      // Upload all attachments one by one
      from(observables).pipe(concatAll()).subscribe(
        {
          next: (progressMessage) => {
            this.isSaving = progressMessage;
          },
          error: (error) => {
            const errorMessage = error.body ? error.body : error.message;
            console.error("Error while saving project", error);
            this.toastService.error("Erreur lors de l'enregistrement du projet", errorMessage);
            this.isSaving = undefined;
            if (project.id) {
              this.router.navigate(["/projects", project.id]);
            } else {
              this.projectForm.enable();
            }
          },
          complete: () => {
            this.toastService.success("Projet enregistré", "Le projet a été enregistré avec succès !");
            this.isSaving = undefined;
            this.router.navigate(["/projects", project.id]);
          }
        });
    });
  }

  private updateProject() {
    if (!this.projectEditCache) {
      this.toastService.error('', "Impossible de mettre à jour le projet.");
      return;
    }

    const formProject = new Project(this.projectForm.value);
    formProject.id = this.projectEditCache.id;
    // Update project
    this.projectsService.updateProject(formProject).subscribe((_) => {
      const observables = [];

      // Change header image if needed (header id is not present)
      if (!this.projectForm.value.headerImage?.id) {
        // Delete existing header image
        const existingHeaderImage = this.projectEditCache?.attachments?.find((a) => a.type === "HEADER_IMAGE");
        if (existingHeaderImage) {
          // Set progress message
          observables.push(
            this.projectsService.deleteAttachment(existingHeaderImage.id)
              .pipe(
                map(() => `Suppression de l'image d'en-tête existante terminée !`)
              )
          );
        }
        observables.push(
          this.projectsService.addAttachment(formProject.id, this.projectForm.value.headerImage, "HEADER_IMAGE", `Image en-tête ${formProject.title}`)
            .pipe(map((event: any) => {
                if (event.type == HttpEventType.UploadProgress) {
                  const uploadProgress = Math.round(100 * (event.loaded / event.total));
                  return `Enregistrement de l'image d'en tête en cours... (${uploadProgress}%)`;
                }
                if (event.type == HttpEventType.Response) {
                  return `Enregistrement de l'image d'en tête terminé !`;
                }
                return `Enregistrement de l'image d'en tête en cours...`;
              }
            ))
        );
      }


      // Change budget file if needed (budget id is not present)
      if (!this.projectForm.value['budgetFile']?.id) {
        // Delete existing budget file
        const existingBudgetFile = this.projectEditCache?.attachments?.find((a) => a.type === "BUDGET");
        if (existingBudgetFile) {
          // Set progress message
          observables.push(
            this.projectsService.deleteAttachment(existingBudgetFile.id)
              .pipe(map(() => `Suppression du budget existant terminée !`))
          );
        }
        observables.push(
          this.projectsService.addAttachment(formProject.id, this.projectForm.value['budgetFile'], "BUDGET", `Budget détaillé`)
            .pipe(map((event: any) => {
                if (event.type == HttpEventType.UploadProgress) {
                  const uploadProgress = Math.round(100 * (event.loaded / event.total));
                  return `Enregistrement du budget détaillé en cours... (${uploadProgress}%)`;
                }
                if (event.type == HttpEventType.Response) {
                  return `Enregistrement du budget détaillé terminé !`;
                }
                return `Enregistrement du budget détaillé en cours...`;
              }
            ))
        );
      }

      // Delete attachments
      const existingAttachments = this.projectEditCache?.attachments?.filter((a) => a.type !== "BUDGET" && a.type !== "HEADER_IMAGE");
      if (existingAttachments) {
        for (let existingAttachment of existingAttachments) {
          if (!this.attachments.find((a) => a.id === existingAttachment.id)) {
            observables.push(
              this.projectsService.deleteAttachment(existingAttachment.id)
                .pipe(
                  map(() => `Suppression de ${existingAttachment.description} terminée !`)
                )
            );
          }
        }
      }

      // Add new attachments
      for (let attachment of this.attachments) {
        if (!attachment.id) {
          observables.push(
            this.projectsService.addAttachment(formProject.id, attachment.file, attachment.type.id, attachment.name).pipe(
              map((event: any) => {
                  if (event.type == HttpEventType.UploadProgress) {
                    const uploadProgress = Math.round(100 * (event.loaded / event.total));
                    return `Enregistrement de ${attachment.name} en cours... (${uploadProgress}%)`;
                  }
                  if (event.type == HttpEventType.Response) {
                    return `Enregistrement de ${attachment.name} terminé !`;
                  }
                  return `Enregistrement de ${attachment.name} en cours...`;
                }
              ))
          );
        }
      }

      // Upload all attachments one by one
      from(observables).pipe(concatAll()).subscribe(
        {
          next: (progressMessage: string) => {
            this.isSaving = progressMessage;
          },
          error: (error) => {
            const errorMessage = error.body ? error.body : error.message;
            console.error("Error while saving project", error);
            this.toastService.error("Erreur lors de l'enregistrement des modifications", errorMessage);
            this.isSaving = undefined;
            this.projectForm.enable();
          },
          complete: () => {
            this.toastService.success("Projet enregistré", "Le projet a été modifié avec succès !");
            this.isSaving = undefined;
            this.router.navigate(["/projects", formProject.id]);
          }
        });
    });
  }
}

import {animate, animateChild, group, query, stagger, state, style, transition, trigger} from '@angular/animations';

export const routerOutletAnimation =
  trigger('routeAnimations', [
    transition('* <=> *', [
      style({position: 'relative'}),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: -30,
          left: 0,
          width: '100%',
          height: '100%',
          opacity: '0%'
        })
      ], {optional: true}),
      query(':enter', [
        style({opacity: '0%'})
      ], {optional: true}),
      query(':leave', animateChild(), {optional: true}),
      group([
        query(':enter', [
          animate('.5s ease', style({opacity: '100%', top: 0}))
        ], {optional: true})
      ]),
      query(':enter', animateChild(), {optional: true}),
    ]),
  ]);


export const inOutHeightAnimation =
  trigger('inOutHeightAnimation', [
    transition(':enter', [
      style({opacity: 0, height: 0}),
      animate('.3s ease-in', style({opacity: 1, height: '*'}))
    ]),
    transition(':leave', [
      style({opacity: 1, height: '*'}),
      animate('.3s ease-out', style({opacity: 0, height: 0}))
    ])
  ]);

export const fadeInAnimation =
  trigger('fadeInAnimation', [
      transition(':enter', [
        style({opacity: 0}), animate('200ms', style({opacity: 1}))]
      ),
      transition(':leave',
        [style({opacity: 1}), animate('200ms', style({opacity: 0}))]
      )
    ],
  );


export const growAnimation =
  trigger('growAnimation', [
      transition(':enter', [
        style({height: 0}), animate('200ms', style({height: '*'}))]
      ),
      transition(':leave',
        [style({height: '*'}), animate('100ms', style({height: 0}))]
      )
    ]
  );

export const expandCollapse =
  trigger('expandCollapse', [
    state('open', style({height: '100%', opacity: 1})),
    state('closed', style({height: 0, opacity: 0})),
    transition('* => *', [animate('300ms')])
  ])

export const slideFromTopAnimation =
  trigger('slideFromTopAnimation', [
      transition(':enter', [
        style({top: 0}), animate('200ms', style({top: '*'}))]
      )
    ]
  );

export const fadeInWithDelayForListsAnimation =
  trigger('fadeInWithDelayForListsAnimation', [
    transition('* => *', [
      query(':enter', [
        style({opacity: 0}),
        stagger(300, [
          animate('0.3s', style({opacity: 1}))
        ])
      ], {optional: true})
    ])
  ]);

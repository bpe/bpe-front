import packageJson from '../../package.json'

export const environment = {
  production: true,
  configFile: 'assets/config/config.prod.json',
  appVersion: packageJson.version
};
